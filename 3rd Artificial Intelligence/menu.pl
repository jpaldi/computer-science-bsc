menu:-
	%iniciar os contadores dos nos
	write('\n----------- Inteligencia Artificial ------------------'),
	nl,
	write('######################################################'),
	nl,
	write('1<= #GALO Expansão de nós MinMax'),
	nl,
	write('2<= #GALO Player vs Agente Inteligente (AlfaBeta)'),
	nl,
	write('3<= #GALO Expansão de nós Alfabeta'),
	nl,
	write('4<= #HEX Expansão de nós MinMax'),
	nl,
	write('5<= #HEX Player vs Agente Inteligente (AlfaBeta)'),
	nl,
	write('6<= #HEX Expansão de nós Alfabeta'),
	nl,
	write('q<= Sair'),
	nl,
	write('Opcao e em seguida enter: '),
	nl,
	read(X),
	subMenu(X).

subMenu(1):- 
	[minimax],
	[galo],
	write('~~~~~~~~~~~~~~~~~~~~~~~~~~'),nl,
	write('~ x -> JOGADOR | o -> COM ~'),nl,
	write('~~~~~~~~~~~~~~~~~~~~~~~~~~'),nl,
	write('Insira o estado a expandir(e(tabuleiro,quem jogou)): '),
	nl,
	read(Estado),
	nl,
	minimax_decidir(Estado,Op),
	escreveOp(Op),
	menu,
	nl.


subMenu(2):-
	[alfabeta],
	[galo],
	write('Primeiro a jogar: Jogador (j) ou COM(c)?'),
	nl,
	read(Jogador),
	nl,
	write('O Jogador vai ser o x!'), nl,
	criar_estado_inicial(Jogador,x,Estado),
	write('Vamos começar!'),nl,
	agente_inteligente(Jogador,Estado),
	nl,
	n(X), write('Nós visitados: '), write(X), nl,
	menu.	

subMenu(3):- 
	[alfabeta],
	[galo],
	write('Insira o grau de profundidade:'),
	nl,
	read(D),
	set_maxdepth(D),
	nl,
	write('Escreva o estado sobre o qual quer aplicar o algoritmo minmax:'),
	nl,
	read(Estado),
	nl,
	minimax_decidir(Estado,Op),
	escreveOp(Op),
	menu.

subMenu(4):- 
	[minimax],
	[hex],
	write('~~~~~~~~~~~~~~~~~~~~~~~~~~'),nl,
	write('~ x -> JOGADOR | o -> COM ~'),nl,
	write('~~~~~~~~~~~~~~~~~~~~~~~~~~'),nl,
	write('Insira o estado a expandir(e(tabuleiro,quem jogou)): '),
	nl,
	read(Estado),
	nl,
	minimax_decidir(Estado,Op),
	escreveOp(Op),
	menu,
	nl.

subMenu(5):-
	[alfabeta],
	[hex],
	write('Primeiro a jogar: Jogador (j) ou COM(c)?'),
	nl,
	read(Jogador),
	nl,
	write('O Jogador vai ser o x!'), nl,
	criar_estado_inicial(Jogador,x,Estado),
	write('Vamos começar!'),nl,
	agente_inteligente(Jogador,Estado),
	nl,
	n(X), write('Nós visitados: '), write(X), nl,
	menu.


subMenu(6):- 
	[alfabeta],
	[hex],
	write('Insira o grau de profundidade:'),
	nl,
	read(D),
	set_maxdepth(D),
	nl,
	write('Escreva o estado sobre o qual quer aplicar o algoritmo minmax:'),
	nl,
	read(Estado),
	nl,
	minimax_decidir(Estado,Op),
	escreveOp(Op),
	menu.

subMenu(q):- fail, !.
