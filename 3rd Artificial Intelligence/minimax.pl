:- dynamic(nos/1).
:- dynamic(prof/1).
:- dynamic(nos_exp/1).

nos(0).
prof(0).
nos_exp(0).

contar_prof(P):-
	retract(prof(_)),
	assertz(prof(P)).
	
contar_nos_visitados(Y):-
	retract(nos(X)),
	Y1 is X + Y,
	assertz(nos(Y1)).	
	
contar_nos_expandidos(Y):-
	retract(nos_exp(X)),
	Y1 is X + Y,
	assertz(nos_exp(Y1)).


minimax_decidir(Ei,terminou):- terminal(Ei).

%Para cada estado sucessor de Ei calcula o valor minimax do estado
%Opf � o operador (jogada) que tem maior valor

minimax_decidir(Ei,Opf):-
	statistics(real_time,[X,_]),
	findall(Es-Op, op1(Ei,Op,Es),L),
	length(L, Tamanho),
	contar_nos_expandidos(Tamanho),
	findall(Vc-Op,(member(E-Op,L), minimax_valor(E,Vc,1)),L1),
	escolhe_max(L1,Opf),
	statistics(real_time,[X1,_]),
	T is X1 - X,
	write('Tempo: '), write(T), nl,
	write('Profundidade: '), prof(Depth), write(Depth), nl,
	write('Nos Visitados: '), nos(Nos), write(Nos), nl,
	write('Nos expandidos: '), nos_exp(Nos_exp), write(Nos_exp),nl,
	write(' '),nl,
	%reinicializar os nos
	retract(prof(_)),
	asserta(prof(0)),
	retract(nos(_)),
	asserta(nos(0)),
	retract(nos_exp(_)),
	asserta(nos_exp(0)).


% se um estado � terminal o valor � dado pela fun��o de utilidade
minimax_valor(Ei,Val,P):- terminal(Ei), valor(Ei,Val,P).

%Se o estado n�o � terminal o valor �:
% -se a profundidade � par, o maior valor dos sucessores de Ei
% -se aprofundidade � impar o menor valor dos sucessores de Ei
minimax_valor(Ei,Val,P):- 
	findall(Es,op1(Ei,_,Es),L),
	length(L, Tamanho),
	contar_nos_expandidos(Tamanho),
	P1 is P+1,
	contar_prof(P1),
	findall(Val1,(member(E,L),minimax_valor(E,Val1,P1)),V),
	length(V, Visitados),
	contar_nos_visitados(Visitados),
	seleciona_valor(V,P,Val).


% Se a profundidade (P) é par, retorna em Val o maximo de V
seleciona_valor(V,P,Val):- X is P mod 2, X=0,!, maximo(V,Val).

% Sen�o retorna em Val o minimo de V
seleciona_valor(V,_,Val):- minimo(V,Val).


maximo([A|R],Val):- maximo(R,A,Val).

maximo([],A,A).
maximo([A|R],X,Val):- A < X,!, maximo(R,X,Val).
maximo([A|R],_,Val):- maximo(R,A,Val).


escolhe_max([A|R],Val):- escolhe_max(R,A,Val).

escolhe_max([],_-Op,Op).
escolhe_max([A-_|R],X-Op,Val):- A < X,!, escolhe_max(R,X-Op,Val).
escolhe_max([A|R],_,Val):- 
	escolhe_max(R,A,Val).


minimo([A|R],Val):- minimo(R,A,Val).

minimo([],A,A).
minimo([A|R],X,Val):- A > X,!, minimo(R,X,Val).
minimo([A|R],_,Val):- 
	minimo(R,A,Val).
