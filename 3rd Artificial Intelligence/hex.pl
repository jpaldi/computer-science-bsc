% estado inicial (tabuleiro, quem jogou)
estado_inicial( e([0,0,0,0,0,
					0,0,0,0,0,
					 0,0,0,0,0,
					  0,0,0,0,0,
					   0,0,0,0,0],o) ).

criar_estado_inicial(j,x,e([0,0,0,0,0,
							0,0,0,0,0,
							0,0,0,0,0,
							0,0,0,0,0,
							0,0,0,0,0],o))
.
criar_estado_inicial(c,x,e([0,0,0,0,0,
							0,0,0,0,0,
							0,0,0,0,0,
							0,0,0,0,0,
							0,0,0,0,0],x)).

estado_terminal( e([Op,Op,Op,Op,Op,
					 _,_,_,_,_,
					   _,_,_,_,_,
					    _,_,_,_,_,
					     _,_,_,_,_],Op)).

estado_terminal( e([_,_,_,_,_,
					 Op,Op,Op,Op,Op,
					  _,_,_,_,_,
					   _,_,_,_,_,
					    _,_,_,_,_],Op)).

estado_terminal( e([_,_,_,_,_,
					 _,_,_,_,_,
					  Op,Op,Op,Op,Op,
					   _,_,_,_,_,
					    _,_,_,_,_],Op)).

estado_terminal( e([_,_,_,_,_,
					 _,_,_,_,_,
					  _,_,_,_,_,
					   Op,Op,Op,Op,Op,
					    _,_,_,_,_],Op)).
estado_terminal( e([_,_,_,_,_,
					 _,_,_,_,_,
					  _,_,_,_,_,
					    _,_,_,_,_,
					    Op,Op,Op,Op,Op],Op)).

estado_terminal( e([Op,_,_,_,_,
					 Op,_,_,_,_,
					  Op,_,_,_,_,
					    Op,_,_,_,_,
					     Op,_,_,_,_],Op)).


estado_terminal( e([_,Op,_,_,_,
					 _,Op,_,_,_,
					  _,Op,_,_,_,
					    _,Op,_,_,_,
					     _,Op,_,_,_],Op)).

estado_terminal( e([_,_,Op,_,_,
					 _,_,Op,_,_,
					  _,_,Op,_,_,
					    _,_,Op,_,_,
					     _,_,Op,_,_],Op)).

estado_terminal( e([_,_,_,Op,_,
					 _,_,_,Op,_,
					  _,_,_,Op,_,
					    _,_,_,Op,_,
					     _,_,_,Op,_],Op)).
estado_terminal( e([_,_,_,Op,_,
					 _,_,_,Op,_,
					  _,_,_,Op,_,
					    _,_,_,Op,_,
					     _,_,_,_,Op],Op)).
estado_empatado( e([A,B,C,D,E,
					F,G,H,I,J,
					 K,L,M,N,O,
					 P,Q,R,S,T,
					  U,V,X,Y,W],_) ):-
	member(A,[x,o]),
	member(B,[x,o]),
	member(C,[x,o]),
	member(D,[x,o]),
	member(E,[x,o]),
	member(F,[x,o]),
	member(G,[x,o]),
	member(H,[x,o]),
	member(I,[x,o]),
	member(J,[x,o]),
	member(K,[x,o]),
	member(L,[x,o]),
	member(M,[x,o]),
	member(N,[x,o]),
	member(O,[x,o]),
	member(P,[x,o]),
	member(Q,[x,o]),
	member(R,[x,o]),
	member(S,[x,o]),
	member(T,[x,o]),
	member(U,[x,o]),
	member(V,[x,o]),
	member(X,[x,o]),
	member(Y,[x,o]),
	member(W,[x,o]).

%terminais
terminal(A):-estado_terminal(A),!.
terminal(A):-estado_empatado(A),!.

valor(e(E,_),1,_):- estado_terminal(e(E,Op1)), Op1=o,!.
valor(E,0,_):- estado_empatado(E),!.
valor(e(E,_),-1,_):- estado_terminal(e(E,Op1)).

op1(e(Ei,o),(x,P),e(Ef,x)):-
	member(P,[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]),
	substituir_indice(Ei,(x,P),Ef).

op1(e(Ei,x),(o,P),e(Ef,o)):-
	member(P,[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25]),
	substituir_indice(Ei,(o,P),Ef).


%substitui Op na posicao de indiceP 
substituir_indice([0|R],(Op,1),[Op|R]):-!.
substituir_indice([X|R],(Op,P),R1):-
	P>1,
	P1 is P-1,
	substituir_indice(R,(Op,P1),R2),
	append([X],R2,R1).

	%desenhar tabuleiro
desenhar_hex(e(L,_)):-
	write('Tabuleiro Actual do Jogo'),
	nl,nl,
	desenhar_valor(L).

desenhar_valor([A,B,C,D,E,
					F,G,H,I,J,
					 K,L,M,N,O,
					 P,Q,R,S,T,
					  U,V,X,Y,W]):-
	write(A),write(' '), write(B),write(' '), write(C),write(' '), write(D), write(' '), write(E),nl,
	nl,
	write(F),write(' '), write(G),write(' '), write(H),write(' '), write(I), write(' '), write(J),nl,
	nl,
	write(K),write(' '), write(L),write(' '), write(M),write(' '), write(N), write(' '), write(O),nl,
	nl,
	write(P),write(' '), write(Q),write(' '), write(R),write(' '), write(S), write(' '), write(T),nl,
	nl,
	write(U),write(' '), write(V),write(' '), write(X),write(' '), write(Y), write(' '), write(W),nl.

inverter('p','j').
inverter('j','p').

agente_inteligente(j,e(L,O)):- estado_terminal(e(L,O)), desenhar_hex(e(L,O)),write('Vencedor: '),write(O),nl,nl.
agente_inteligente(j,e(L,O)):- estado_empatado(e(L,O)), desenhar_hex(e(L,O)),write('Empate!!'),nl,nl.

agente_inteligente(p,e(L,O)):- estado_terminal(e(L,O)), desenhar_hex(e(L,O)),write('Vencedor: '),write(O),nl,nl.
agente_inteligente(p,e(L,O)):- estado_empatado(e(L,O)), desenhar_hex(e(L,O)),write('Empate!!'),nl,nl.

agente_inteligente(p,e(L,O)):-
	desenhar_hex(e(L,O)),
	nl,nl,
	minimax_decidir(e(L,O),Op),
	escreveOp(Op),
	nl,
	nl,
	op1(e(L,O),Op,Es),
	inverter(p,Jogador1),
	agente_inteligente(Jogador1,Es).

%se for o jogador a jogar
agente_inteligente(j,e(L,O)):-
	desenhar_hex(e(L,O)),
	nl,
	nl,
	write('Escreva a posicao onde deseja jogar '),
	read(Pos),
	op1(e(L,O),(_,Pos),Es),
	inverter(j,Jogador1),
	agente_inteligente(Jogador1,Es).


%escrever operador
escreveOp(terminou):-!.
escreveOp((P,Op)):-nl,
	write('melhor jogada com a peça: '),write(P),
	nl,
	write('colocar na posicao: '),write(Op),nl.


