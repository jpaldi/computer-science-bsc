% estado inicial (tabuleiro, quem jogou)

criar_estado_inicial(j,x,e([0,0,0,0,0,0,0,0,0],o)).

criar_estado_inicial(c,x,e([0,0,0,0,0,0,0,0,0],x)).

%terminais
terminal(A):-estado_terminal(A),!.
terminal(A):-estado_empatado(A),!.


estado_terminal( e([Op,Op,Op, _,_,_, _,_,_],Op) ).
estado_terminal( e([_,_,_, Op,Op,Op, _,_,_],Op) ).
estado_terminal( e([_,_,_, _,_,_, Op,Op,Op],Op) ).


estado_terminal( e([Op,_,_, Op,_,_, Op,_,_],Op) ).
estado_terminal( e([_,Op,_, _,Op,_, _,Op,_],Op) ).
estado_terminal( e([_,_,Op, _,_,Op, _,_,Op],Op) ).


estado_terminal( e([Op,_,_, _,Op,_, _,_,Op],Op) ).
estado_terminal( e([_,_,Op, _,Op,_, Op,_,_],Op) ).

estado_empatado( e([A,B,C, D,E,F, G,H,I],_) ):-
	member(A,[x,o]),
	member(B,[x,o]),
	member(C,[x,o]),
	member(D,[x,o]),
	member(E,[x,o]),
	member(F,[x,o]),
	member(G,[x,o]),
	member(H,[x,o]),
	member(I,[x,o]).

% função de utilidade, retorna o valor dos estados terminais, 1 ganha , 0 empata, -1 perde
valor(e(E,_),1,_):- estado_terminal(e(E,Op1)), Op1=o,!.
valor(E,0,_):- estado_empatado(E),!.
valor(e(E,_),-1,_):- estado_terminal(e(E,Op1)).

%operadores
%op1(Estado,(Simbolo,Posicao),Estado Seguinte)
op1(e(Ei,o),(x,P),e(Ef,x)):-
	member(P,[1,2,3,4,5,6,7,8,9]),
	substituir_indice(Ei,(x,P),Ef).

op1(e(Ei,x),(o,P),e(Ef,o)):-
	member(P,[1,2,3,4,5,6,7,8,9]),
	substituir_indice(Ei,(o,P),Ef).


%substitui Op na posicao de indiceP 
substituir_indice([0|R],(Op,1),[Op|R]):-!.
substituir_indice([X|R],(Op,P),R1):-
	P>1,
	P1 is P-1,
	substituir_indice(R,(Op,P1),R2),
	append([X],R2,R1).



% função de avaliacao
% avalia(Estado, Tipo_peca, Avaliacao)
avalia(E,Op,3):-
	terminal(E).

avalia(e(E,Op),Op,2):-
	E = [Op,Op,X, _,_,_, _,_,_],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [ _,_,_, Op,Op,X,_,_,_],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [_,_,_, _,_,_, Op,Op,X],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [X,Op,Op, _,_,_, _,_,_],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [ _,_,_, X,Op,Op,_,_,_],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [_,_,_, _,_,_, X,Op,Op],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [Op,_,_, Op,_,_, X,_,_],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [_,Op,_, _,Op,_, _,X,_],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [_,_,Op, _,_,Op, _,_,X],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [X,_,_, Op,_,_, Op,_,_],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [_,X,_, _,Op,_, _,Op,_],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [_,_,X, 
	     _,_,Op, 
	     _,_,Op],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [Op,_,_, 
	     _,Op,_, 
	     _,_,X],
	X \= Op.

avalia(e(E,Op),Op,2):-
	E = [_,_,X, 
		 _,Op,_,
		 Op,_,_],
	X \= Op.

avalia(e(E,Op),Op,1).

inverte_peca('x','o').
inverte_peca('o','x').

%desenhar tabuleiro
desenhar_galo(e(L,_)):-
	write('Tabuleiro Actual do Jogo'),
	nl,nl,
	desenhar_valor(L).

desenhar_valor([A,B,C,D,E,F,G,H,I]):-
	write(A),write(' '), write(B),write(' '), write(C),nl,
	nl,
	write(D),write(' '), write(E),write(' '), write(F),nl,
	nl,
    write(G),write(' '), write(H),write(' '), write(I),nl.


%trocar o jogador que é a
inverter('p','j').
inverter('j','p').

agente_inteligente(j,e(L,O)):- estado_terminal(e(L,O)), desenhar_galo(e(L,O)),write('Vencedor: '),write(O),nl,nl.
agente_inteligente(j,e(L,O)):- estado_empatado(e(L,O)), desenhar_galo(e(L,O)),write('Empate!!'),nl,nl.

agente_inteligente(p,e(L,O)):- estado_terminal(e(L,O)), desenhar_galo(e(L,O)),write('Vencedor: '),write(O),nl,nl.
agente_inteligente(p,e(L,O)):- estado_empatado(e(L,O)), desenhar_galo(e(L,O)),write('Empate!!'),nl,nl.

agente_inteligente(p,e(L,O)):-
	desenhar_galo(e(L,O)),
	nl,nl,
	minimax_decidir(e(L,O),Op),
	escreveOp(Op),
	nl,
	nl,
	op1(e(L,O),Op,Es),
	inverter(p,Jogador1),
	agente_inteligente(Jogador1,Es).

%se for o jogador a jogar
agente_inteligente(j,e(L,O)):-
	desenhar_galo(e(L,O)),
	nl,
	nl,
	write('Escreva a posicao onde deseja jogar '),
	read(Pos),
	op1(e(L,O),(_,Pos),Es),
	inverter(j,Jogador1),
	agente_inteligente(Jogador1,Es).


%escrever operador
escreveOp(terminou):-!.
escreveOp((P,Op)):-nl,
	write('melhor jogada com a peça: '),write(P),
	nl,
	write('colocar na posicao: '),write(Op),nl.




