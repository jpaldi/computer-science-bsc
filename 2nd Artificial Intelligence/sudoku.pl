dimensao(9).

estado_inicial(e(L,L1)) :-	dimensao(N),
							dominio(N,D),
							L1 = [v(c(1,2),D,1),
								  v(c(1,6),D,8),
								  v(c(1,8),D,7),
								  v(c(1,9),D,3),
								  v(c(2,4),D,5),
								  v(c(2,6),D,9),
								  v(c(3,1),D,7),
								  v(c(3,7),D,9),
								  v(c(3,9),D,4),
								  v(c(4,6),D,4),
								  v(c(5,5),D,3),
								  v(c(5,6),D,5),
								  v(c(5,8),D,1),
								  v(c(5,9),D,8),
								  v(c(6,1),D,8),
								  v(c(6,4),D,9),
								  v(c(7,4),D,7),
								  v(c(8,1),D,2),
								  v(c(8,2),D,6),
								  v(c(8,5),D,4),
								  v(c(8,8),D,3),
								  v(c(9,3),D,5),
								  v(c(9,6),D,3)],
								  ger(N,D,L,L1).
							
ger(N,D,L,L1) :-	estado(N,D,L,1,1,L1).



estado(N,_,[],I,_,_) :- I>N,!.
estado(N,D,R,I,J,L1) :- estadoJ(N,D,R1,I,J,L1),
                     	I1 is I + 1,
                     	estado(N,D,R2,I1,J,L1),
                     	append(R1, R2,R),!.

estadoJ(N,_D,[],_I,J, _) :- J>N,!.
estadoJ(N,D,[v(c(I,J),D,_)|R],I,J, L1) :- J1 is J + 1,
									  \+(member(v(c(I,J),_,_),L1)),!,
                                      estadoJ(N,D,R,I,J1,L1).
estadoJ(N,D,R,I,J,L1) :- J1 is J + 1,
                      	 estadoJ(N,D,R,I,J1,L1).
dominio(0,[]):-!.
dominio(N,D) :- N1 is N - 1,
				dominio(N1,D1),
				append(D1,[N],D).

ve_restricoes(e(_,Afect)):- 
	linhas(Afect, 1,9),
	colunas(Afect, 1,9),
	ver_quadrante(Afect, 1,9),
	ver_quadrante(Afect, 2,9),
	ver_quadrante(Afect, 3,9),
	ver_quadrante(Afect, 4,9),
	ver_quadrante(Afect, 5,9),
	ver_quadrante(Afect, 6,9),
	ver_quadrante(Afect, 7,9),
	ver_quadrante(Afect, 8,9),
	ver_quadrante(Afect, 9,9).



linhas(_, I , N) :- I>N, !.
linhas(Afect, I, N) :-  findall(A,member(v(c(I,_),_,A),Afect),L1),fd_all_different(L1),
					 	I1 is I + 1,
					 	linhas(Afect, I1, N).

colunas(_, J, N) :- J>N, !.
colunas(Afect, J,N) :- findall(A,member(v(c(_,J),_,A),Afect),L1),fd_all_different(L1),
				       J1 is J + 1,
				       colunas(Afect, J1, N).


ver_quadrante(_, Q,N):- Q>N,!.
ver_quadrante(Afect, Q,_):-
    findall(A,(member(v(c(I,J),_,A),Afect),calc_quadrante(I,J,Q)),L1), 
    fd_all_different(L1).

calc_quadrante(I,J,Q) :- Row is (J-1) // 3,
						 Col is (I-1) // 3,
						 Q is (Col * 3) + Row + 1.


%imprimir a solução
esc(L):-sort(L,L1),
		nl,nl,
		write(L1),nl,
		dimensao(D),
		esc1(L1,D,1).

esc1([],_,_).
esc1([v(_,_,V)|R],X,Y):-
	X=:=Y,
	A is 1,
	dimensao(D),
	esc(D,V,1),
	esc1(R,X,A).

esc1([v(_,_,V)|R],X,Y):-
	X\=Y,
	A is Y+1,
	dimensao(D),
	esc2(D,V,1),
	esc1(R,X,A).

esc2(_,V,_):-
	write(V),
	write(' ').

esc(_,V,_):-
	write(V),
	nl.