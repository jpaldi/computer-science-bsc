dimensao(4).

estado_inicial(e(L,[])) :-	dimensao(N),
							ger(N,L).
ger(N,L) :- N1 is N * N,
			dominio(N1,D),
			estado(N,D,L,1,1).

numero_magico(N, X) :- M is (N*(N*N + 1)) / 2,
					   X is round(M).

dominio(0,[]):-!.
dominio(N,D) :- N1 is N - 1,
				dominio(N1,D1),
				append(D1,[N],D).

estado(N,_,[],I,_) :- I>N,!.
estado(N,D,R,I,J) :- estadoJ(N,D,R1,I,J),
                     I1 is I + 1,
                     estado(N,D,R2,I1,J),
                     append(R1, R2,R).

estadoJ(N,_D,[],_I,J) :- J>N,!.
estadoJ(N,D,[v(c(I,J),D,_)|R],I,J) :- J1 is J + 1,
                                      estadoJ(N,D,R,I,J1).

ve_restricoes(E):-
%	write(E),nl,
	%E = e(_,[v(c(I,J),_,V)|R]),
	E = e(_,Afect),
	Afect = [v(c(I,J),_,_)|_],
	findall(A,member(v(c(I,_),_,A),Afect),L1),fd_all_different(L1),
	findall(A,member(v(c(_,J),_,A),Afect),L2),fd_all_different(L2),
	findall(A,member(v(c(_,_),_,A),Afect),L3),fd_all_different(L3),
	length(L1, T1),
	length(L2, T2),
	check_sum(L1,T1),
	check_sum(L2,T2).


check_sum(_,D):-
	dimensao(ND),
	D<ND,!.

check_sum(L,D) :- dimensao(D),
				  numero_magico(D,N),
				  list_sum(L,NN),
				  NN = N.

list_sum([], 0).
list_sum([Item1| Tail], Total) :-
    list_sum(Tail, NTotal),
    Total is NTotal + Item1.

%imprimir a solução
esc(L):-sort(L,L1),
		nl,nl,
		write(L1),nl,
		dimensao(D),
		esc1(L1,D,1).

esc1([],_,_).
esc1([v(_,_,V)|R],X,Y):-
	X=:=Y,
	A is 1,
	dimensao(D),
	esc(D,V,1),
	esc1(R,X,A).

esc1([v(_,_,V)|R],X,Y):-
	X\=Y,
	A is Y+1,
	dimensao(D),
	esc2(D,V,1),
	esc1(R,X,A).

esc2(_,V,_):-
	write(V),
	write(' ').

esc(_,V,_):-
	write(V),
	nl.