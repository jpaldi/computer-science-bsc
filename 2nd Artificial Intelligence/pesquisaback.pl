p(Prob):- consult(Prob),estado_inicial(E0), back(E0,A), esc(A).

back(e([],A),A).
back(E,Sol):- sucessor(E,E1), 
			  ve_restricoes(E1),
              back(E1,Sol).

sucessor(e([v(N,D,V)|R],E),e(R,[v(N,D,V)|E])):- member(V,D).


f(Prob):-consult(Prob),estado_inicial(E0), forward(E0,A), esc(A).

forward(e([],A),A).

forward(E,Sol):- sucessor(E,E1),
				 ve_restricoes(E1),
				 %actualizar o dominio
	             forward_checking(E1,E2),
	             forward(E2,Sol).


forward_checking(e(NAfect,[v(N,D,V)|Afect]),e(NAfectS,[v(N,D,V)|Afect])):- actualizaDom(V, NAfect, NAfectS).

actualizaDom(_,[],[]).
actualizaDom(V,[v(N,D,_)|NAfect],[v(N,DS,_)|NAfectS]):- delete(D,V,DS),
														actualizaDom(V, NAfect, NAfectS).
