/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;

/**
 * Jersey REST client generated for REST resource:application
 * [questionario/cria]<br>
 * USAGE:
 * <pre>
 *        NewJerseyClientCria client = new NewJerseyClientCria();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author jpaldi
 */
public class NewJerseyClientCria {

    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:9200/so2/";

    public NewJerseyClientCria() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("questionario/cria");
    }

    /**
     * @param x query parameter
     * @param n query parameter
     * @param p1 query parameter
     * @param p2 query parameter
     * @param p3 query parameter
     * @param p4 query parameter
     * @param p5 query parameter
     * @return response object (instance of responseType class)
     */
    public void criarQuestionario(String x, String n, String p1, String p2, String p3, String p4, String p5) throws ClientErrorException {
        String[] queryParamNames = new String[]{"x", "n", "p1", "p2", "p3", "p4", "p5"};
        String[] queryParamValues = new String[]{x, n, p1, p2, p3, p4, p5};
        ;
        javax.ws.rs.core.Form form = getQueryOrFormParams(queryParamNames, queryParamValues);
        javax.ws.rs.core.MultivaluedMap<String, String> map = form.asMap();
        for (java.util.Map.Entry<String, java.util.List<String>> entry : map.entrySet()) {
            java.util.List<String> list = entry.getValue();
            String[] values = list.toArray(new String[list.size()]);
            webTarget = webTarget.queryParam(entry.getKey(), (Object[]) values);
        }
        webTarget.request().post(null);
    }

    private Form getQueryOrFormParams(String[] paramNames, String[] paramValues) {
        Form form = new javax.ws.rs.core.Form();
        for (int i = 0; i < paramNames.length; i++) {
            if (paramValues[i] != null) {
                form = form.param(paramNames[i], paramValues[i]);
            }
        }
        return form;
    }

    public void close() {
        client.close();
    }
    
}
