/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import questionarios.MediasList;
import questionarios.QuestList;

/**
 *
 * @author jpaldi
 */
public class MediasServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            NewJerseyClientMedias jm = new NewJerseyClientMedias();
            NewJerseyClient jc = new NewJerseyClient();
            String nomeQ = request.getParameter("nomeQ");
            MediasList medias = jm.medias_JSON(MediasList.class,nomeQ);
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<link rel=\"stylesheet\" href=\"CSS/style_criar1.css\">");
            out.println("<head>");
            out.println("<title>Servlet MediasServlet</title>");            
            out.println("</head>");
            out.println("<body>");
                                    out.println("        <div>\n" +
                "              <h1>João Aldeano(30395) & André Lourenço(31593)</h1>\n" +
                "        </div>\n" +
                "        <div class=\"navmenu\">\n" +
                "            <div class=\"navlink\">\n" +
                "                <a href=\"index.html\">HOME</a>\n" +
                "\n" +
                "                <a href=\"criar_questionario.html\">Criar</a>\n" +
                "\n" +
                "                <a href=\"lista\">Listar</a>\n" +
                "\n" +
                "                <a href=\"apagar_questionario.html\">Apagar</a>\n" +
                "\n" +
                "                <a href=\"responder_questionario.html\">Responder</a>\n" +
                "\n" +
                "                <a href=\"medias.html\">Médias</a> \n" +
                "            </div>\n" +
                "        </div>");
            QuestList ql= jc.listaQuestionarios_JSON(QuestList.class);
            out.println("<h2 align=\"center\" class=\"perguntaslindas\"> Médias do questionário: "+nomeQ+"</h2>");
            boolean encontrou =false;
                for (int i=0; i<ql.size(); i++){
                String [] x = ql.lista.get(i).getNome().split(" ");
                if (x[0].equals(nomeQ)){
                    out.println("<div align=\"center\">");
                    out.println("<p>Questionario     |   Média<p> <br>");
                    if(ql.lista.get(i).perguntas.size()==3){                                               
                            out.println("<form action=\"respondida\">\n" +
                            "<p>\n" +
                             ql.lista.get(i).perguntas.get(0).getPergunta()+ "<br>" + medias.perguntas.get(0) + "<br>"+
                             ql.lista.get(i).perguntas.get(1).getPergunta()+"<br>" + medias.perguntas.get(1) + "<br>"+
                             ql.lista.get(i).perguntas.get(2).getPergunta()+"<br>"  + medias.perguntas.get(2) + "<br>"+
                            "</p>\n" + "</form>");
                            out.println("</div>");
                            out.println("</form>");
                            out.println("</body>");
                            out.println("</html>");
                            encontrou = true;
                    }
                               
      
                    else if(ql.lista.get(i).perguntas.size()==4){
                                                                                  
                                                                             
                            out.println("<form action=\"respondida\">\n" +
                            "<p>\n" +
                             ql.lista.get(i).perguntas.get(0).getPergunta()+"<br>" + medias.perguntas.get(0) + "<br>"+
                             ql.lista.get(i).perguntas.get(1).getPergunta()+"<br>" + medias.perguntas.get(1) +  "<br>"+
                             ql.lista.get(i).perguntas.get(2).getPergunta()+"<br>"+ medias.perguntas.get(2) +  "<br>"+
                             ql.lista.get(i).perguntas.get(3).getPergunta()+"<br>"  + medias.perguntas.get(3) +  "<br>"+    
                            "</p>\n" + "</form>");
                            out.println("</div>");
                            out.println("</body>");
                            out.println("</html>");
                            encontrou = true;
                    }
                     
                    else if(ql.lista.get(i).perguntas.size()==5){
                                                                                                     
                            out.println("<form action=\"respondida\">\n" +
                            "<p>\n" +
                             ql.lista.get(i).perguntas.get(0).getPergunta()+ "<br>" + medias.perguntas.get(0) + "<br>"+
                             ql.lista.get(i).perguntas.get(1).getPergunta()+"<br>" + medias.perguntas.get(1) +  "<br>"+
                             ql.lista.get(i).perguntas.get(2).getPergunta()+"<br>" + medias.perguntas.get(2) +  "<br>"+
                             ql.lista.get(i).perguntas.get(3).getPergunta()+"<br>" + medias.perguntas.get(3) +  "<br>"+
                             ql.lista.get(i).perguntas.get(4).getPergunta()+"<br>" + medias.perguntas.get(4) +  "<br>"+  
                            "</p>\n" + "</form>");

                            out.println("</div>");
                            out.println("</body>");
                            out.println("</html>");
                            encontrou = true;
                    }
                  
                }
            }
            if(encontrou==false){
            out.println("<h2 align=\"center\" class=\"perguntaslindas\">Questionário não existente. </h2>");
            out.println("</body>");
            out.println("</html>");
        }
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
