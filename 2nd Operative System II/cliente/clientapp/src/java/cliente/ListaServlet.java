/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import questionarios.QuestList;

/**
 *
 * @author jpaldi
 */
@WebServlet(name = "ListaServlet", urlPatterns = {"/lista"})
public class ListaServlet extends HttpServlet {
    NewJerseyClient jc = new NewJerseyClient();

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            QuestList ql = new QuestList();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"CSS/lista_style.css\">");
            out.println("<title>Servlet ListaServlet</title>");            
            out.println("</head>");
            out.println("<body>");
                        out.println("        <div>\n" +
                "       <h1>João Aldeano(30395) & André Lourenço(31593)</h1>\n" +
                "        </div>\n" +
                "        <div class=\"navmenu\">\n" +
                "            <div class=\"navlink\">\n" +
                "                <a href=\"index.html\">HOME</a>\n" +
                "\n" +
                "                <a href=\"criar_questionario.html\">Criar</a>\n" +
                "\n" +
                "                <a href=\"lista\">Listar</a>\n" +
                "\n" +
                "                <a href=\"apagar_questionario.html\">Apagar</a>\n" +
                "\n" +
                "                <a href=\"responder_questionario.html\">Responder</a>\n" +
                "\n" +
                "                <a href=\"medias.html\">Médias</a> \n" +
                "            </div>\n" +
                "        </div>");
            ql = jc.listaQuestionarios_JSON(QuestList.class);
            out.println("<div class=\"listaqs\" align=\"center\">");
            out.println("<p>Questionario     |    Respondido<p> <br>");
            for(int i=0; i<ql.size(); i++){
                out.println("<p>"+ql.lista.get(i).getNome()+"   |   "+ql.lista.get(i).respondido+"<p> <br>");
            }
             out.println(" <button id=\"goback\"onclick=\"goBack()\">Voltar</button>\n" +
                                        "\n" +
                                        "<script>\n" +
                                        "function goBack() {\n" +
                                        "    window.history.back();\n" +
                                        "}\n" +
                                        "</script>");
            out.println("<div>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
