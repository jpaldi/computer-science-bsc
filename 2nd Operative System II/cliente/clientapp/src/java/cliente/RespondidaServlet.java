/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jpaldi
 */
public class RespondidaServlet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
        protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            NewJerseyClient jc = new NewJerseyClient();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"CSS/style_criar1.css\">");
            out.println("<title>Servlet RespondidaServlet</title>");            
            out.println("</head>");
            out.println("<body>");
                                     out.println("        <div>\n" +
                "             <h1>João Aldeano(30395) & André Lourenço(31593)</h1>\n" +
                "        </div>\n" +
                "        <div class=\"navmenu\">\n" +
                "            <div class=\"navlink\">\n" +
                "                <a href=\"index.html\">HOME</a>\n" +
                "\n" +
                "                <a href=\"criar_questionario.html\">Criar</a>\n" +
                "\n" +
                "                <a href=\"lista\">Listar</a>\n" +
                "\n" +
                "                <a href=\"apagar_questionario.html\">Apagar</a>\n" +
                "\n" +
                "                <a href=\"responder_questionario.html\">Responder</a>\n" +
                "\n" +
                "                <a href=\"medias.html\">Médias</a> \n" +
                "            </div>\n" +
                "        </div>");
            String resposta1 = request.getParameter("resposta1");
            String resposta2 = request.getParameter("resposta2");
            String resposta3 = request.getParameter("resposta3");
            String resposta4 = request.getParameter("resposta4");
            String resposta5 = request.getParameter("resposta5");
            String sizeQ = request.getParameter("sizeQuest");
            String nomeQ = request.getParameter("nomeQuest");
            if (sizeQ.equals("3")){
                out.println("<h2 class=\"perguntaslindas\"> Questionário respondido com sucesso</h2>");
                out.println("<form action=\"/index.html\">");
                out.println("</body>");
                out.println("</html>");
                jc.responder(nomeQ, sizeQ, resposta1,resposta2,resposta3,resposta4,resposta5);
            }
            else if (sizeQ.equals("4")){
                out.println("<h2 class=\"perguntaslindas\"> Questionário respondido com sucesso</h2>");
                out.println("</form>");
                out.println("</body>");
                out.println("</html>");
                jc.responder(nomeQ, sizeQ, resposta1,resposta2,resposta3,resposta4,resposta5);
            }
            
            else if (sizeQ.equals("5")){
                out.println("<h2 class=\"perguntaslindas\"> Questionário respondido com sucesso</h2>");
                out.println("<h2>"+resposta1+"</h2>");
                out.println("<h2>"+resposta2+"</h2>");
                out.println("<h2>"+resposta3+"</h2>");
                out.println("</form>");
                out.println("</body>");
                out.println("</html>");
                jc.responder(nomeQ, sizeQ, resposta1,resposta2,resposta3,resposta4,resposta5);
            }
            else{
                out.println("<h2 class=\"perguntaslindas\"> Erro ao responder ao questionário.</h2>");
                out.println("</form>");
                out.println("</body>");
                out.println("</html>");   
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
