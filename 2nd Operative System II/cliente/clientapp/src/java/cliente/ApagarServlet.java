/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import questionarios.QuestList;

/**
 *
 * @author jpaldi
 */
public class ApagarServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            NewJerseyClient jc = new NewJerseyClient();
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"CSS/lista_style.css\">");
            out.println("<title>Servlet ApagarServlet</title>");            
            out.println("</head>");
            out.println("<body>");
                         out.println("        <div>\n" +
                "                  <h1>João Aldeano(30395) & André Lourenço(31593)</h1>" +
                "        </div>\n" +
                "        <div class=\"navmenu\">\n" +
                "            <div class=\"navlink\">\n" +
                "                <a href=\"index.html\">HOME</a>\n" +
                "\n" +
                "                <a href=\"criar_questionario.html\">Criar</a>\n" +
                "\n" +
                "                <a href=\"lista\">Listar</a>\n" +
                "\n" +
                "                <a href=\"apagar_questionario.html\">Apagar</a>\n" +
                "\n" +
                "                <a href=\"responder_questionario.html\">Responder</a>\n" +
                "\n" +
                "                <a href=\"medias.html\">Médias</a> \n" +
                "            </div>\n" +
                "        </div>");
            QuestList ql = new QuestList();
            String nomeQ = request.getParameter("nomeQ");
            ql = jc.listaQuestionarios_JSON(QuestList.class);
            boolean encontrou = false;
            for (int i=0; i<ql.size(); i++){
                String [] x = ql.lista.get(i).getNome().split(" ");
                if (x[0].equals(nomeQ)){
                    jc.apagar(nomeQ);
                    out.println("<h3>APAGADO QUESTIONARIO</h3>");
                    out.println("</body>");
                    out.println("</html>");
                    encontrou = true;
                }
            }
            
            if(!encontrou){
                out.println("<h3>Questionário não existente. </h1>");
                out.println("</body>");
                out.println("</html>");
            }
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
