/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import questionarios.Questionario;

/**
 *
 * @author jpaldi
 */
public class CriaServlet extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<link rel=\"stylesheet\" href=\"CSS/style_criar1.css\">");
            out.println("<title>Servlet CriaServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("        <div>\n" +
                "           <h1>João Aldeano(30395) & André Lourenço(31593)</h1>" +
                "        </div>\n" +
                "        <div class=\"navmenu\">\n" +
                "            <div class=\"navlink\">\n" +
                "                <a href=\"index.html\">HOME</a>\n" +
                "\n" +
                "                <a href=\"criar_questionario.html\">Criar</a>\n" +
                "\n" +
                "                <a href=\"lista\">Listar</a>\n" +
                "\n" +
                "                <a href=\"apagar_questionario.html\">Apagar</a>\n" +
                "\n" +
                "                <a href=\"responder_questionarios.html\">Responder</a>\n" +
                "\n" +
                "                <a href=\"medias.html\">Médias</a> \n" +
                "            </div>\n" +
                "        </div>");
            NewJerseyClientCria jc = new NewJerseyClientCria();
            String nomeQ = request.getParameter("nomeQ");
            String perguntaQ = request.getParameter("perguntaQ");
            String pergunta1 = request.getParameter("pergunta1");
            String pergunta2 = request.getParameter("pergunta2");
            String pergunta3 = request.getParameter("pergunta3");
            String pergunta4 = request.getParameter("pergunta4");
            String pergunta5 = request.getParameter("pergunta5");
            
            if(nomeQ!=null){
                switch(perguntaQ){
                    case "3":
                        if(pergunta1!=null && pergunta2!=null && pergunta3!=null){
                            Questionario x = new Questionario(nomeQ,3);
                            x.adicionar_pergunta(pergunta1);
                            x.adicionar_pergunta(pergunta2);
                            x.adicionar_pergunta(pergunta3);
                            jc.criarQuestionario(nomeQ, perguntaQ, pergunta1,pergunta2,pergunta3,pergunta4,pergunta5); 
                            out.println("<h3> Questionario com 3 perguntas criado </h1>");
                            out.println("</body>");
                            out.println("</html>");
                        }
                        else{
                            out.println("<h3> ERRO AO CRIAR QUESTIONARIO </h3>");
                            out.println("</html>");
                        }
                        break;
                    case "4":
                        if(pergunta1!=null && pergunta2!=null && pergunta3!=null && pergunta4!=null){
                            Questionario y = new Questionario(nomeQ,4);
                            y.adicionar_pergunta(pergunta1);
                            y.adicionar_pergunta(pergunta2);
                            y.adicionar_pergunta(pergunta3);
                            y.adicionar_pergunta(pergunta4);
                            jc.criarQuestionario(nomeQ, perguntaQ, pergunta1,pergunta2,pergunta3,pergunta4,pergunta5); 
                            out.println("<h3> Questionario com 4 perguntas criado </h3>");
                            out.println("</body>");
                            out.println("</html>");
                        }
                        else{
                            out.println("<h3> ERRO AO CRIAR QUESTIONARIO </h3>");
                            out.println("</body>");
                            out.println("</html>");
                        }
                        break;
                    case "5":
                        if(pergunta1!=null && pergunta2!=null && pergunta3!=null && pergunta4!=null && pergunta5 != null){
                            Questionario w = new Questionario(nomeQ,5);
                            w.adicionar_pergunta(pergunta1);
                            w.adicionar_pergunta(pergunta2);
                            w.adicionar_pergunta(pergunta3);
                            w.adicionar_pergunta(pergunta4);
                            w.adicionar_pergunta(pergunta5);
                            jc.criarQuestionario(nomeQ, perguntaQ, pergunta1,pergunta2,pergunta3,pergunta4,pergunta5); 
                            out.println("<h3> Questionario com 5 perguntas criado </h3>");
                            out.println("</body>");
                            out.println("</html>");
                        }
                        else{
                            out.println("<h3> ERRO AO CRIAR QUESTIONARIO </h3>");
                            out.println(" <button id=\"goback\"onclick=\"goBack()\">Voltar</button>\n" +
                           "\n" +
                           "<script>\n" +
                           "function goBack() {\n" +
                           "    window.history.back();\n" +
                           "}\n" +
                           "</script>");
                            out.println("</body>");
                            out.println("</html>");
                        }
                        break; 
                    default:
                        out.println("<h3> ERRO AO CRIAR QUESTIONARIO </h3>");
                        out.println(" <button id=\"goback\"onclick=\"goBack()\">Voltar</button>\n" +
                           "\n" +
                           "<script>\n" +
                           "function goBack() {\n" +
                           "    window.history.back();\n" +
                           "}\n" +
                           "</script>");
                        out.println("</body>");
                        out.println("</html>");
            }
        }
        else{
                
            out.println("<h3> ERRO AO CRIAR QUESTIONARIO </h3>");
            //voltar atras
            out.println("</body>");
            out.println("</html>"); 
        }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
