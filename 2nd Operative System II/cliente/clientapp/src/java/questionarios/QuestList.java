package questionarios;

import java.util.Vector;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import questionarios.Questionario;


@XmlAccessorType(XmlAccessType.FIELD)

@XmlRootElement(name = "listaquestionarios")
public class QuestList {
    @XmlElement(required = true)
    public Vector<Questionario> lista;
    
    public QuestList(){
        lista = new Vector<Questionario>();
    }
    
    public void adicionar_questionario(Questionario x){
        lista.add(x);
    }
    
    public int size(){
        return lista.size();
    }
}
