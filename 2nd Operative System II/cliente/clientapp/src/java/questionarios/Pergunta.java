/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionarios;

/**
 *
 * @author jpaldi
 */

import java.util.Formatter;
import java.util.Vector;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pergunta", propOrder = {
    "pergunta",
    "resposta"
})
@XmlRootElement(name = "pergunta")
public class Pergunta{
    private static short MAX_RESPOSTAS = 1000;
    
    @XmlElement(required = true)
    protected String pergunta;
    @XmlElement(required = true)
    protected Vector<Integer> resposta;
    
    public Pergunta(){}
    
    public Pergunta(String pg) {
	this.pergunta= pg;
	this.resposta = new Vector<Integer>(MAX_RESPOSTAS);
    }
  
    public String getPergunta() {
        return pergunta;
    }
    
    public void responder(int x){
        if(x>=1 && x<= 10){
            resposta.add(x);
        }
        else{
            //resposta fora dos limites
        }
    }
    
    public float media(){
        int soma = 0;
        for(int i = 0; i<resposta.size();i++){
            soma += resposta.get(i);
        }
        
        return soma/resposta.size();
        
    }

}