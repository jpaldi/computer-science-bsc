/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionarios;

import java.util.Vector;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)

@XmlRootElement(name = "listaperguntas")
public class MediasList {
    @XmlElement(required = true)
    public Vector<Double> perguntas;
    
    public MediasList(){
        perguntas = new Vector<Double>();
    }
    
    public void adicionar_medias(Double x){
        perguntas.add(x);
    }
    
    public int size(){
        return perguntas.size();
    }
}