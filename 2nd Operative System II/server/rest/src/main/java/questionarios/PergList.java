package questionarios;

import java.util.Vector;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import questionarios.Questionario;


@XmlAccessorType(XmlAccessType.FIELD)

@XmlRootElement(name = "listaperguntas")
public class PergList {
    @XmlElement(required = true)
    public Vector<Pergunta> perguntas;
    
    public PergList(){
        perguntas = new Vector<Pergunta>();
    }
    
    public void adicionar_pergunta(Pergunta x){
        perguntas.add(x);
    }
    
    public int size(){
        return perguntas.size();
    }
}