/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questionarios;

/**
 *
 * @author jpaldi
 */

import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)

@XmlRootElement(name = "questionario")
public class Questionario {
    
    private static int MIN_PERGUNTAS =3;
    private static int MAX_PERGUNTAS =5;
    
    @XmlElement(required = true)
    protected Vector<Pergunta> perguntas;
    @XmlElement(required = true)
    private String nome;
    @XmlElement(required = true)
    int numero_de_perguntas;
    @XmlElement(required = true)
    public int respondido;

    public Questionario(){}
    
    public Questionario(String nome, int n){
        if (n>=MIN_PERGUNTAS && n<=MAX_PERGUNTAS){
            this.nome = nome;
            perguntas = new Vector<Pergunta>(n);
            numero_de_perguntas = n;
            respondido = 0;
        }
        else{
            //impossivel criar
        }
    }

    public Questionario(Questionario x, int n) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getNome(){
        return this.nome;
    }
    public void adicionar_pergunta(String x){
        if(perguntas.size()<this.numero_de_perguntas && perguntas.size()< MAX_PERGUNTAS){
            this.perguntas.add(new Pergunta(x));
        }
        else{
            System.out.println("Este questionario so pode ter " +this.numero_de_perguntas+ " perguntas.");
        }
    }
    
    public void obter_perguntas(){
        if(perguntas.size()<this.numero_de_perguntas){
            System.out.println("Este questionario tem que ter no minimo " + this.numero_de_perguntas + " perguntas.");
        }
        else{
            short i = 0;
            while (i < this.perguntas.size()){
                //imprimir as perguntas
                System.out.println(perguntas.get(i).pergunta);
                i++;
            }   
        }
    }
    
    public void obter_media(){
        if(perguntas.size()<this.numero_de_perguntas){
            System.out.println("Este questionario tem que ter no minimo " + this.numero_de_perguntas + " perguntas.");
        }
        else{
            float media;
            short i = 0;
            while (i < this.perguntas.size()){
                //imprimir as perguntas
                System.out.println(perguntas.get(i).pergunta);
                media = perguntas.get(i).media();
                System.out.println("\n media: "+ media + "\n");
                i++;
            }   
        }
    }
    
    public void responder_perguntas(int resposta, int i){
        this.perguntas.get(i).responder(resposta);
    }
    
    public int respondido(){
        return this.respondido;
    }
    
    public void incrementa_respondido(){
        this.respondido += 1;
    }

}