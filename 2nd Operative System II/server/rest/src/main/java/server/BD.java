/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import questionarios.MediasList;
import questionarios.PergList;
import questionarios.Pergunta;
import questionarios.QuestList;
import questionarios.Questionario;

public class BD{
    Connection con = null;
    Statement stmt = null;
    
    public BD(String dbhost, String db, String user, String pwd){
        try {
            Class.forName ("org.postgresql.Driver");  
            
            // url = "jdbc:postgresql://host:port/database",
            con = DriverManager.getConnection("jdbc:postgresql://"+dbhost+":5432/"+db,
                                              user,
                                              pwd);
            
            stmt = con.createStatement();
            //System.out.println("CONNECTED :) ");

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Erro na ligação à BD " + e);
        }
    }
    
    public void reporBD(){
        String s;
        StringBuilder sb = new StringBuilder();

        try {
            FileReader fr = new FileReader(new File("comandos.sql"));

            BufferedReader br = new BufferedReader(fr);

            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
            br.close();

            String[] inst = sb.toString().split(";");

            for (int i = 0; i < inst.length; i++) {
                if (!inst[i].trim().equals("")) {
                    stmt.executeUpdate(inst[i]);
                    //System.out.println(">>" + inst[i]); //Mostra no terminal as instruções SQL executadas 

                }
            }
            System.out.println("[Client] Base de Dados reposta.");

        } catch (Exception e) {
            System.out.println("*** Erro : " + e.toString());
            e.printStackTrace();
            System.out.println(sb.toString());
        }
    }
    
    public QuestList listaQuestionarios(){
        QuestList lista_quest = new QuestList();

        try {
            ResultSet rs = stmt.executeQuery("Select * FROM questionario;");
            while (rs.next()) {

                String nome = rs.getString("nome");
                int perguntas = rs.getInt("numperguntas");
                int rp = rs.getInt("respondido");
                Questionario x = new Questionario(nome,perguntas);
                x.respondido = rp;
                lista_quest.adicionar_questionario(x);                             
            }
            rs = stmt.executeQuery("Select * FROM pergunta;");
            while (rs.next()) {

                String nome = rs.getString("nomeQuest");
                String pergunta = rs.getString("pergunta");
                
                for (int ind = 0;ind < lista_quest.size(); ind++){
                    if(lista_quest.lista.get(ind).getNome().equals(nome)){
                        lista_quest.lista.get(ind).adicionar_pergunta(pergunta);
                    }
                }                             
            }
            
            System.out.println("[Client] Questionarios listados");
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Erro ao aceder à BD");
        }
        return lista_quest;
    }
    
     public void novoQuestionario(String nome, int numPerguntas){
        try{
            stmt.executeUpdate("INSERT INTO questionario VALUES('" + nome + "', " + numPerguntas +","+0+");");
            System.out.println("[Client] Criado questionario " + nome + " com " + numPerguntas + " perguntas.");
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("Erro de criaçao de questionário.");
        }
    }
     
    public void addPergunta(String questionario, String pergunta,int i){
        try{
            stmt.executeUpdate("INSERT INTO pergunta VALUES('" + questionario + "','" 
                    + pergunta + "',"+i+");");
            System.out.println("[Client] Adicionada pergunta ao questionario " + questionario + ".");
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("Erro de adição de pergunta.");
        }
    }
    
    public PergList listarPerguntas(String questionario){
        PergList lista_perguntas = new PergList();
        try {
            ResultSet rs = stmt.executeQuery("Select pergunta FROM pergunta WHERE "
                    + "nomeQuest = '"+questionario+"';");
            while (rs.next()) {

                String pg = rs.getString("pergunta");
                Pergunta x = new Pergunta(pg);
                lista_perguntas.adicionar_pergunta(x);
                return lista_perguntas;
            }

            System.out.println("[Client] Perguntas Listadas");
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Erro ao aceder à BD");
        }
        return null;
    }
    
    public void apagar(String nomeQuest){
        try{
            stmt.executeUpdate("DELETE FROM questionario WHERE nome = '" + nomeQuest +"' ;");
            System.out.println("[Client] Questionario " + nomeQuest + " apagado.");
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("Erro ao aceder à BD");
        }
    }
    
    
    public MediasList medias(String nomeQuest){
        try{
            ResultSet rs = stmt.executeQuery("SELECT numPerguntas FROM questionario WHERE nome = '"+nomeQuest+"';");
            int num;
            
            if(rs.next())
                num = rs.getInt("numPerguntas");
               
            else
                return null;
            
            int total = 0;
            MediasList medias = new MediasList();
            
            for(int i = 1; i < num + 1; i++){
                double val = 0;
                rs = stmt.executeQuery("SELECT * FROM resposta WHERE "
                        + "nomeQuest = '"+nomeQuest+"' "
                        + "AND numero ="+i+";");
                
                while(rs.next()){
                    total = total + rs.getInt("frequencia");
                    val = val + (rs.getInt("frequencia") * rs.getInt("resposta"));
                }

                val = val / total;
                medias.adicionar_medias(val);
                total = 0;
            }            
            return medias;
            
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("Erro ao aceder à BD");
            
            return null;
        }
    }
    
    
    public void responder(String nomeQuest, int i, int resposta) throws SQLException{
            try {
                                
                boolean encontrou = false;
                ResultSet rs = stmt.executeQuery("Select numero,resposta FROM resposta WHERE "
                        +"nomeQuest = '"+ nomeQuest +"';");

                while (rs.next()) {
                    int rp = (Integer)rs.getObject("resposta");
                    int numb = (Integer)rs.getObject("numero");
                    if(rp == resposta && numb==i){
                         stmt.executeUpdate("UPDATE resposta SET frequencia = frequencia + 1"
                         + "WHERE nomeQuest = '"+ nomeQuest + "' AND numero = "+ i +
                          " AND resposta = "+ resposta + ";");
                         encontrou = true;
                         break;
                     }
                }
                if (!encontrou){
                    stmt.executeUpdate("INSERT INTO resposta VALUES('" 
                                        + nomeQuest + "\', " + i +","+resposta+",1);");
                }
                rs.close();
                
            }
            catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Erro ao aceder à BD");
            }
    }
    
    public void incrementaRespondido(String nomeQuest) throws SQLException{
        stmt.executeUpdate("UPDATE questionario SET respondido = respondido + 1 WHERE nome = '"+nomeQuest+"';");
    }
    
    
}   
