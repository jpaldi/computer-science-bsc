/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;
import static com.sun.jersey.api.model.Parameter.Source.PATH;
import questionarios.QuestList;

import com.sun.jersey.spi.resource.Singleton;
import java.io.FileInputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.QueryParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import questionarios.MediasList;
import questionarios.PergList;
import questionarios.Questionario;


@Singleton
@Path(value = "/questionario")
public class QuestionarioRes {

    public QuestList questList = new QuestList();
    
    Properties properties = new Properties();
    BD bd;
    
    public QuestionarioRes(){
        try {
            properties.load(new FileInputStream("properties.properties"));
        } catch (IOException e) {
            System.out.println("Erro ao ler ficheiro .properties");
        }
        String dbhost = properties.getProperty("dbhost");
        String db = properties.getProperty("db");
        String user = properties.getProperty("user");
        String pwd = properties.getProperty("pwd");
        bd = new BD(dbhost,db,user,pwd);
        
        /* DEPOIS APAGAR ESTA MERDA */
        bd.reporBD();//
        //////////////
        
    }
    
    //listar os questionarios
    @GET
    @Produces({"application/json", "application/xml"})
    public synchronized QuestList listaQuestionarios() {
        questList =  bd.listaQuestionarios();
        return questList;
    }
    
    @Path("/medias")
    @GET
    @Produces({"application/json", "application/xml"})
    public synchronized MediasList medias(@QueryParam("x") String x) {
        return bd.medias(x);
    }

    @POST
    @Path("/cria")
    @Consumes({"application/json","application/xml"})
    public synchronized void criarQuestionario(@QueryParam("x") String x, 
                                               @QueryParam("n") int n,
                                               @QueryParam("p1") String p1,
                                               @QueryParam("p2") String p2,
                                               @QueryParam("p3") String p3,
                                               @QueryParam("p4") String p4,
                                               @QueryParam("p5") String p5) {
        if (x != null && n >2 && n < 6){
            switch (n){
                case 3:
                    System.out.println("ENTROU criar questionario 3");
                    bd.novoQuestionario(x, n);
                    bd.addPergunta(x, p1, 1);
                    bd.addPergunta(x, p2, 2);
                    bd.addPergunta(x, p3, 3);
                    
                    Questionario q = new Questionario(x,n);
                    q.adicionar_pergunta(p1);
                    q.adicionar_pergunta(p2);
                    q.adicionar_pergunta(p3);
                    questList.adicionar_questionario(q);
                    break;
                case 4:
                    bd.novoQuestionario(x, n);
                    bd.addPergunta(x, p1, 1);
                    bd.addPergunta(x, p2, 2);
                    bd.addPergunta(x, p3, 3);
                    bd.addPergunta(x, p4, 4);
                    
                    Questionario q4 = new Questionario(x,n);
                    q4.adicionar_pergunta(p1);
                    q4.adicionar_pergunta(p2);
                    q4.adicionar_pergunta(p3);
                    q4.adicionar_pergunta(p4);
                    questList.adicionar_questionario(q4);
                    break;
                case 5:
                    bd.novoQuestionario(x, n);
                    bd.addPergunta(x, p1, 1);
                    bd.addPergunta(x, p2, 2);
                    bd.addPergunta(x, p3, 3);
                    bd.addPergunta(x, p4, 4);
                    bd.addPergunta(x, p5, 5);
                    
                    Questionario q5 = new Questionario(x,n);
                    q5.adicionar_pergunta(p1);
                    q5.adicionar_pergunta(p2);
                    q5.adicionar_pergunta(p3);
                    q5.adicionar_pergunta(p4);
                    q5.adicionar_pergunta(p5);
                    questList.adicionar_questionario(q5);
                    break;
            }
        }
    }
    
    @POST
    @Consumes({"application/xml"})
    public synchronized void responder(@QueryParam("x") String x,
                                       @QueryParam("size") int size,
                                       @QueryParam("r1") int r1,
                                       @QueryParam("r2") int r2,
                                       @QueryParam("r3") int r3,
                                       @QueryParam("r4") int r4,
                                       @QueryParam("r5") int r5) throws SQLException{
        
        if (size >2 && size < 6){
            switch(size){
                case 3:
                    bd.responder(x,1,r1);
                    bd.responder(x,2,r2);
                    bd.responder(x,3,r3);
                    bd.incrementaRespondido(x);
                    break;
                case 4:
                    bd.responder(x,1,r1);
                    bd.responder(x,2,r2);
                    bd.responder(x,3,r3);
                    bd.responder(x,4,r4);
                    bd.incrementaRespondido(x);
                    break;
                case 5:
                    bd.responder(x,1,r1);
                    bd.responder(x,2,r2);
                    bd.responder(x,3,r3);
                    bd.responder(x,4,r4);
                    bd.responder(x,5,r5);
                    bd.incrementaRespondido(x);
                    break;
            }
        }
    }
    
    @DELETE
    @Produces({"application/json", "application/xml"})
    @Consumes({"application/json", "application/xml"})
    public synchronized void apagar(@QueryParam("nome") String nome) {
        bd.apagar(nome);
        questList.apagar_questionario(nome);
    }
    
    
}

