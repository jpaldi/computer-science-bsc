DROP TABLE IF EXISTS questionario CASCADE;
CREATE TABLE questionario (
  nome CHAR(64),
  numPerguntas INTEGER,
  respondido INTEGER,
  PRIMARY KEY (nome)
);

DROP TABLE IF EXISTS pergunta CASCADE;
CREATE TABLE pergunta (
  nomeQuest CHAR(64),
  pergunta CHAR(256),
  numero INTEGER,
  FOREIGN KEY (nomeQuest) REFERENCES questionario(nome) ON DELETE CASCADE,
  PRIMARY KEY (nomeQuest, numero)
);

DROP TABLE IF EXISTS resposta CASCADE;
CREATE TABLE resposta (
    nomeQuest CHAR(64),
    numero INTEGER,
    resposta INTEGER,
    frequencia INTEGER,
    FOREIGN KEY (nomeQuest) REFERENCES questionario(nome) ON DELETE CASCADE,
    PRIMARY KEY(nomeQuest, numero, resposta)
);

INSERT INTO questionario 
	VALUES ('informatica',3,0);

INSERT INTO questionario 
	VALUES ('sporting',4,0);

INSERT INTO questionario 
	VALUES ('alentejo',4,0);

INSERT INTO pergunta 
	VALUES ('informatica','Gosta das cadeiras?',1);

INSERT INTO pergunta 
	VALUES ('informatica','Gosta do curso?',2);

INSERT INTO pergunta 
	VALUES ('informatica','Gosta de programar?',3);

INSERT INTO pergunta 
	VALUES ('sporting','Gosta do sporting?',1);

INSERT INTO pergunta 
	VALUES ('sporting','Gosta do Bruno de Carvalho?',2);

INSERT INTO pergunta 
	VALUES ('sporting','Gosta de futebol?',3);

INSERT INTO pergunta 
	VALUES ('sporting','Gosta de alvalade?',4);

INSERT INTO pergunta 
	VALUES ('alentejo','Gosta de cante alentejano?',1);

INSERT INTO pergunta 
	VALUES ('alentejo','Gosta dos seistetos?',2);

INSERT INTO pergunta 
	VALUES ('alentejo','Gosta da UE?',3);

INSERT INTO pergunta 
	VALUES ('alentejo','Gosta de Açorda?',5);



