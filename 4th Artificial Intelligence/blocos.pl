estado_inicial([mao_livre(direita),
				mao_livre(esquerda),
				bloco_grande(a),
				bloco_grande(d),
				bloco_pequeno(b),
				bloco_pequeno(c),
				mesa(a),
				mesa(b),
				em_cima_grande(d,a,direita),
				em_cima_grande(d,a,esquerda),
				em_cima_pequeno(c,b),
				so(c),
				nadaemcima(d),
				livre(c),
				livre(d)
				]).

%estado_inicial([mesa(a), livre(a),so(a), bloco_pequeno(a), bloco_pequeno(b), mesa(b), 
%				livre(b), so(b),mao_livre(direita),mao_livre(esquerda)]).

%estado_final([mesa(b), em_cima_pequeno(a,b), so(a)]).

estado_final([mesa(d),em_cima_grande(c,d,direita)]).

%Pegar num bloco pequeno de cima de outro bloco pequeno 
accao(agarrar_pequeno1(X,M),
	 [livre(X), mao_livre(M),so(X),em_cima_pequeno(X,Y),bloco_pequeno(Y),bloco_pequeno(X)], 
	 [bloco_mao(X,M),livre(Y),so(Y)], 
	 [mao_livre(M), livre(X),em_cima_pequeno(X,Y),so(X)]).

%Pegar num bloco pequeno ques esta junto a outro em de cima da mesa 
accao(agarrar_pequeno2(X,M),
	 [mao_livre(M),bloco_pequeno(X),bloco_pequeno(Y),juntos(X,Y),juntos(Y,X), livre(X), livre(Y),mesa(X),mesa(Y)], 
	 [bloco_mao(X,M),so(Y)], 
	 [mao_livre(M), juntos(X,Y),juntos(Y,X), livre(X),mesa(X)]).

%Pegar num bloco pequeno que está sozinho de cima da mesa 
accao(agarrar_pequeno3(X,M),
	 [livre(X), mao_livre(M),
	 mesa(X),so(X),bloco_pequeno(X)], 
	 [bloco_mao(X,M)], 
	 [mao_livre(M), livre(X),mesa(X), so(X)]).




%Pegar num bloco pequeno que está junto a outro em cima de um grande 
accao(agarrar_pequeno4(X,M),
	 [mao_livre(M), bloco_pequeno(X), em_cima_grande(X,Z,_), 
	  em_cima_grande(Y,Z,_), 
	 bloco_pequeno(Y), bloco_grande(Z),juntos(X,Y),juntos(Y,X)], 
	 [bloco_mao(X,M), so(Y)], 
	 [em_cima_grande(X,Z,_), mao_livre(M), livre(X),juntos(X,Y),juntos(Y,X)]).

%Pegar num bloco pequeno que está sozinho em cima de um grande 
accao(agarrar_pequeno5(X,M),
	 [mao_livre(M), bloco_pequeno(X), em_cima_grande(X,Z,_),
	  livre(X), 
	 	so(X), bloco_grande(Z)], 
	 [bloco_mao(X,M), livre(Z)], 
	 [em_cima_grande(X,Z,_), mao_livre(M), livre(X),so(X),nadaemcima(Z)]).

%Largar um bloco pequeno sozinho na mesa
accao(largar_pequeno_mesa(X,M),
	 [bloco_pequeno(X), bloco_mao(X,M)], 
	 [mesa(X), so(X), mao_livre(M), livre(X)], 
	 [bloco_mao(X,M)]).

%Largar um bloco pequeno colado a outro pequeno na mesa
accao(largar_juntos_mesa(X,Y,M),
	 [bloco_pequeno(X), bloco_mao(X,M), 
	 so(Y), livre(Y), bloco_pequeno(Y), mesa(Y)], 
	 [mesa(X), mao_livre(M), livre(X), 
	 juntos(X,Y),juntos(Y,X)], 
	 [bloco_mao(X,M), so(Y)]).

%Largar um bloco pequeno em cima de um pequeno
accao(largar_pequeno1(X,Y,M),
	 [bloco_pequeno(X), bloco_mao(X,M), 
	 bloco_pequeno(Y), so(Y),livre(Y)], 
	 [mao_livre(M), livre(X), em_cima_pequeno(X,Y),so(X)], 
	 [bloco_mao(X,M), so(Y), livre(Y), livre(X),bloco_mao(X,M)]).

%Largar um bloco pequeno em cima de um grande do lado esquerdo junto a outro
accao(largar_pequeno2(X,Y,M,esquerda),
	 [bloco_mao(X,M),bloco_pequeno(X),bloco_grande(Y),
	 	em_cima_grande(B,Y,direita), livre(Y)], 
	 [mao_livre(M), livre(X), 
	 em_cima_grande(X,Y,esquerda), 
	 juntos(X,B),juntos(B,X)], 
	 [bloco_mao(X,M), so(B), bloco_mao(X,M)]).


%Largar um bloco pequeno em cima de um grande do lado direito junto a outro
accao(largar_pequeno3(X,Y,M,direita),
	 [bloco_mao(X,M),bloco_pequeno(X)
	 		,bloco_grande(Y),
	 	em_cima_grande(B,Y,esquerda), so(B), livre(B)], 
	 
	 [mao_livre(M), livre(X), 
	 em_cima_grande(X,Y,direita), juntos(X,B),
	 juntos(B,X)], 
	 [bloco_mao(X,M), so(B), bloco_mao(X,M)]).


%Largar um bloco pequeno em cima de um grande que não tem nada em cima
accao(largar_pequeno4(X,Y,M,L),
	 [bloco_mao(X,M),bloco_pequeno(X),bloco_grande(Y),
	 	nadaemcima(Y), livre(Y)], 
	 [mao_livre(M), livre(X), em_cima_grande(X,Y,L)], 
	 [bloco_mao(X,M), so(X), bloco_mao(X,M),nadaemcima(Y)]).

%Pegar um bloco grande do cima da mesa
accao(agarrar_grande1(X),
	 [mao_livre(direita), mao_livre(esquerda),
	  mesa(X), bloco_grande(X), nadaemcima(X),livre(X)], 
	 [bloco_mao(X, direita), bloco_esquerda(X,esquerda)], 
	 [mesa(X), mao_livre(direita),mao_livre(esquerda),nadaemcima(X),livre(X)]).

%Pegar um bloco grande de cima de outro grande
accao(agarrar_grande2(X,Y),
	 [mao_livre(direita), mao_livre(esquerda), bloco_grande(X), em_cima_grande(X,Y,direita),
	 	em_cima_grande(X,Y,esquerda), bloco_grande(Y), livre(X)], 
	 [bloco_mao(X, direita),bloco_mao(X,esquerda), nadaemcima(Y)], 
	 [em_cima_grande(X,Y,direita),	em_cima_grande(X,Y,esquerda), mao_livre(direita),mao_livre(esquerda),livre(X)]).


%Pegar um bloco grande de cima de dois pequenos juntos
accao(agarrar_grande_juntos(X,Y),
	 [mao_livre(direita), mao_livre(esquerda), bloco_grande(X), em_cima_pequeno(X,Y), em_cima_pequeno(X,Z),
	 	juntos(Y,Z),juntos(Z,Y), bloco_pequeno(Y),bloco_pequeno(Z), livre(X), nadaemcima(X)], 
	 [bloco_mao(X,direita), bloco_mao(x,esquerda),livre(Y),livre(Z)], 
	 [mao_livre(direita),mao_livre(esquerda),nadaemcima(X),livre(X), em_cima_pequeno(X,Y), em_cima_pequeno(X,Z)]).

%Largar um bloco grande na mesa
accao(largar_grande_mesa(X),
	 [bloco_grande(X), bloco_mao(X,direita), bloco_mao(X,esquerda)], 
	 [mesa(X), nadaemcima(X), mao_livre(direita),mao_livre(esquerda), livre(X)], 
	 [bloco_mao(X,direita),bloco_mao(X,esquerda)]).

%Largar grande em cima de grande
accao(largar_grande_mesa(X,Y),
	 [bloco_grande(X), bloco_mao(X,direita), bloco_mao(X,esquerda), bloco_grande(Y), nadaemcima(Y),livre(Y)], 
	 [mao_livre(direita),mao_livre(esquerda), livre(X),em_cima_grande(X,Y,direita),nadaemcima(X),em_cima_grande(X,Y,esquerda)], 
	 [bloco_mao(X,direita),bloco_mao(X,esquerda),livre(Y), nadaemcima(Y)]).

%Largar grande em cima de dois pequenos juntos
accao(largar_grande_juntos(X,P1,P2),
	 [bloco_grande(X), bloco_pequeno(P1), bloco_pequeno(P2), juntos(P1,P2),juntos(P2,P1),livre(P1),livre(P2),
	  	bloco_mao(X,direita), bloco_mao(X,esquerda)], 
	 [mao_livre(direita),mao_livre(esquerda), livre(X),em_cima_pequeno(X,P1),em_cima_pequeno(X,P2),
	 	nadaemcima(X)], 
	 [bloco_mao(X,direita),bloco_mao(esquerda),livre(P1), livre(P2), juntos(P1,P2), juntos(P2,P1)]).