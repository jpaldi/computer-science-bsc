.data 
file:			.asciiz "lena512color.rgb" #file name
fileblackandwhite:	.asciiz "lena512blackandwhite.gray" #file name
final:			.asciiz "lena512final.gray" #final
asc_str:		.asciiz "********************TRABALHO PRATICO ASCI****************\n...Loading...\n"
end_string:		.asciiz "FIM.\n"
debugging:		.asciiz "Esta parte executou.\n"


.text

######################################     MAIN      ##################################################################

main:
	la $a0 , asc_str 	#carrega para a0 a string asc_str loading
	jal print 		#chama a função que imprime
	nop
	
	la $a0, file  		#carrega para a0 a string com o nome do ficheiro rgb
	li $a1, 0		#o ficheiro é apenas para leitura apenas
	jal openfile		#chama a função openfile
	nop
	
	move $a0, $v0		#passa para a a0 o valor de retorno da função anterior
	
	la $a1, 0x100100a0	#Endereço para o qual Ler(lena512color.rgb)
	li $a2,786432		#numero de bytes que queremos ler(512x512x3)
	
	jal read_rgb_image	#chama a função que carrega
	nop
	 
	
	la $a0, 0x100100a0	#carrega o endereço de imagem em rgb  
	addi $a1, $a0, 786432	#numero de bytes 512x512x3
	jal rgb_to_gray		#chama a função rgb_to_gray
	nop
	
	
	la $a0, fileblackandwhite 	#carrega para a0 a string com o nome do novo ficheiro
	li $a1, 0x100100a0 		#carrega para a1 o endereço do primeiro byte da imagem rgb
	addi $a1, $a1, 786432 		#incrementa 512x512x3 bytes (agora a1 tem o endereço do primeiro byte da img black and white
	addi $a2, $zero, 262144 	#carrega para a2 o valor de uma imagem em gray 512x512
	jal write_gray_image 		#chama a função write_gray_image 
	nop

	la $a0, file		#carrega para a0 a string com a imagem rgb
	jal closefile		#chama a função closefile
	nop
	
	li $a0, 0x100100a0 	#carrega para a0 o endereço de do primeiro byte da imagem em rgb  
	addi $a0, $a0, 786432 	#carrega para a0 o endereço do primeiro byte da imagem blackandwhite
	li $a1, 262144 		#tamanho total da imagem blackandwhite(512x512)
	li $a2, 512  		#tamanho de cada linha 
	
	jal convolution
	nop
				#(ver diagrama no relatório com a utilização da memória)
	li $a0, 0x10010a00	#carrega para a0 o endereço do primeiro byte da imagem em rgb  
	addi $a0, $a0, 786432 	#carrega para a0 o endereço do primeiro byte da imagem blackandwhite  

	addi $a0, $a0, 262144 	#carrega para a0 o endereço do primeiro byte da imagem alterada com o op. sobel horizontal
	addi $a1, $a0, 262144 	#carrega para a1 o endereço do primeiro byte da imagem alterada com o op. sobel vertical 
	addi $a2, $a1, 262144 	#carrega para a2 o endereço onde vamos guardar o primeiro byte da imagem final
	
	jal contour
	nop
	
	# ESCREVER O FICHEIRO FINAL
	la $a0, final		#carrega para a0 a string com o nome do ficheiro rgb
	li $a1, 1		#o ficheiro é para escrever
	jal openfile		#chama a função openfile
	nop

	move $a0, $v0		#passa para a a0 o valor de retorno da função anterior		
	la $a1, 0x100100a0	#carrega para a1 o endereço do primeiro byte da imagem em rgb  
	addi $a1, $a1, 786432 	#carrega para a1 o endereço do primeiro byte da imagem blackandwhite
	addi $a1, $a1, 262144	#carrega para a1 o endereço do primeiro byte da imagem alterada com o op. sobel horizontal
	addi $a1, $a1, 262144 	#carrega para a1 o endereço do primeiro byte da imagem alterada com o op. sobel vertical 
	addi $a1, $a1, 262144	#carrega para a2 o endereço onde vamos guardar o primeiro byte da imagem final
	li $a2, 262144 		#quantos bytes queremos escrever no ficheiro(512x512)
	
	jal writefile		#chama a função writefile
	nop
	
	la $a0, final		#carrega em a0 a string com o nome do ficheiro final
	jal closefile		#fecha o ficheiro
	nop
	
	j end 			#saltar para o fim do programa
	nop


##############################################################################################
#função write_gray_image
#argumentos: a0: string com o nome do ficheiro; a1: buffer address ; a2: tamanho do ficheiro
write_gray_image:

	addi $sp,$sp-4	#Reservar espaço na pilha
	sw $ra,0($sp)	#Guardar ra na pilha
	
	move $s0, $a0  	#carrega para s0, a string com o nome do ficheiro
	move $s1, $a1	#carrega para s1, o buffer address da imagem em gray
	move $s2, $a2	#carrega para s2, o tamanho do ficheiro

	li $a1, 1	# Abrir o ficheiro para escrita
	jal openfile
	nop
	
	move $a0, $v0	#carrega em v0 o valor de retorno da função anterior
	move $a1, $s1	#repõe o argumento original
	move $a2, $s2	#repõe o argumento original
	
	jal writefile	#salta para função writefile
	nop
	
	move $a0, $s0	#repõe o a0 original
	jal closefile
	nop
	
	lw $ra,0($sp) 	#Carregar novamente o ra
	addi $sp,$sp,4	#Libertar espaço na pilha
	
	jr $ra		#saltar para ra
	nop
	
									
#função convulution
# a0 = buffer address da imagem preto e branco
# a1 = tamanho total da imagem
# a2 = tamanho da linha
convolution:
	addi $sp,$sp-4		#Reservar espaço na pilha
	sw $ra,0($sp)		#Guardar ra na pilha
	
	move $s0, $a0  		#buffer address da imagem blackandwhite
	move $s1, $a1  		#tamanho da imagem
	move $s2, $a2 		#tamanho da cada linha da "matriz"
	
	add $a1, $a0, $s1 #e
	
	jal multiplica_horizontal
	nop
	
	move $a0, $s0
	add $a1, $a0, $s1  #escrever a matriz vertical a frente da grey 
	add $a1, $a1, $s1  #escrever a matriz vertical a frente da horizontal 
	move $a2, $s2
	
	jal multiplica_vertical
	nop
	
	lw $ra,0($sp) 	#Carregar novamente o ra
	addi $sp,$sp,4	#Libertar espaço na pilha
	
	jr $ra
	nop
	
	
	

			
									
#função para convulsionar operador sobel horizontal
# a0 - buffer address gray
# a1 - buffer address multiplica horizontal
# a2 - tamanho da linha
multiplica_horizontal:
	
	addi $sp,$sp-4	#Reservar espaço na pilha
	sw $ra,0($sp)	#Guardar ra na pilha
	
	move $t7, $a1 
	
	mul $t8, $a2, $a2 
	add $t7, $t7, $t8 #para escrever a frente 512x512 bytes
	
	subi $t7, $t7, 1
	sub $t7, $t7, $a2 #para o ciclo acabar quando chegar ao penultimo pixel da penultima linha
	
	add $a1, $a1 , $a2 #ignorar a primeira linha
	addi $a1, $a1, 1
	
	add $t6, $zero, $zero #contador para auxiliar a matriz, quando chegar ao antepenultimo elemento da linha da imagem gray
	                      #avança para a proxima linha e igual com a imagem a com op sobel ignora os contornos
	
	add $t9, $zero, $zero #guardar resultado
mh_ciclo:	
	
	addi $t6, $t6 , 1
	
	bge $a1, $t7, mh_sair 	#verifica se ja lemos todos os pixeis
	nop
	
	subi $t4, $a2, 1 
	bge $t6, $t4, incrementa_a0_a1 #se a imagem gray vai na antepenultima linha salta para a seguinte e ignora a ultima linha da img sobel
	nop
	
	
	lbu $t0, 0($a0)
	addi $a0, $a0, 2
	lbu $t2, 0($a0)
	
				#vamos guardar a multiplicaçao no registo t9
	add $t9, $zero, $t0		
	mul $t2, $t2, -1
	add $t9, $t9, $t2
					
	add $a0, $a0, $a2 	#avançar para a próxima linha imagem gray
	addi $a0, $a0, -2	#voltar a linha inicial 3x3
	
	lbu $t0, 0($a0)                 
	addi $a0, $a0, 2 		
	lbu $t2, 0($a0)			
	
	mul $t0, $t0, 2
	add $t9, $t9 , $t0		
	mul $t2, $t2, -2
	add $t9, $t9, $t2
	
	add $a0, $a0, $a2	
	addi $a0, $a0, -2
	
	lbu $t0, 0($a0)
	addi $a0, $a0, 2
	lbu $t2, 0($a0)	
	
	add $t9, $t9 , $t0		
	mul $t2, $t2, -1
	add $t9, $t9, $t2
	
	div $t9, $t9, 4
	abs $t9, $t9
	sb $t9, 0($a1)
			
	sub $a0, $a0, $a2
	sub $a0, $a0, $a2
	sub $a0, $a0, 2   #voltar a linha de cima
	
	addi $a0, $a0 , 1
	addi $a1, $a1, 1 #avançar nas imgs
	
	j mh_ciclo
	nop
	
mh_sair:	
	lw $ra,0($sp) 	#Carregar novamente o ra
	addi $sp,$sp,4	#Libertar espaço na pilha
	
	jr $ra		#retorna ao sitio que chamou a função
	nop	
			

	
#função para convulsionar operador sobel vertical
# a0 - buffer address gray
# a1 - buffer address multiplica vertical
# a2 - tamanho da linha		
			
multiplica_vertical:
	
	addi $sp,$sp-4	#Reservar espaço na pilha
	sw $ra,0($sp)	#Guardar ra na pilha
	
	move $t7, $a1 
	
	mul $t8, $a2, $a2 
	add $t7, $t7, $t8
	
	subi $t7, $t7, 1
	sub $t7, $t7, $a2 #para o ciclo acabar quando chegar ao penultimo pixel da penultima linha
	
	add $a1, $a1 , $a2
	addi $a1, $a1, 1
	
	add $t6, $zero, $zero #contador para auxiliar a matriz, quando chegar ao antepenultimo elemento da linha da imagem blackandwhite
	                      # avança para a proxima linha e igual com a imagem a com op sobel ignora os contornos
	add $t9, $zero, $zero #guardar resultado
mv_ciclo:	
	
	addi $t6, $t6 , 1
	
	bge $a1, $t7, mv_sair 	#verifica se ja lemos todos os pixeis
	nop
	
	subi $t4, $a2, 1 
	bge $t6, $t4, incrementa_a0_a1 #se a imagem gray vai na antepenultima linha salta para a seguinte e ignora a ultima linha da img sobel
	nop
	
	
	lbu $t0, 0($a0)
	addi $a0, $a0, 1
	lbu $t1, 0($a0)
	addi $a0, $a0, 1
	lbu $t2, 0($a0)
	
	#operaçoes com matrizes
	#vamos guardar a multiplicaçao no registo t9
	add $t9, $zero, $t0	#multiplicação dos primeiros 3 elementos
	mul $t1, $t1, 2
	add $t9, $t9, $t1
	add $t9, $t9, $t2
					
									
	add $a0, $a0, $a2 	#avançar para a próxima linha imagem gray
	add $a0, $a0, $a2 	#avançar para a próxima linha imagem gray
	addi $a0, $a0, -2	#voltar ao inicio da linha

	lbu $t0, 0($a0)
	addi $a0, $a0, 1        
	lbu $t1, 0($a0)
	addi $a0, $a0, 1
	lbu $t2, 0($a0)
	
	mul $t0, $t0, -1     
	add $t9, $t9, $t0
	mul $t1, $t1, -2
	add $t9, $t9, $t1
	mul $t2, $t2, -1
	add $t9, $t9, $t2

	div $t9, $t9, 4
	abs $t9, $t9
	sb $t9, 0($a1)
			
	sub $a0, $a0, $a2
	sub $a0, $a0, $a2
	sub $a0, $a0, 2   #voltar a linha de cima
	
	addi $a0, $a0 , 1
	addi $a1, $a1, 1 #avançar nas imgs
	
	j mv_ciclo
	nop
	
mv_sair:	
	lw $ra,0($sp) 	#Carregar novamente o ra
	addi $sp,$sp,4	#Libertar espaço na pilha
	
	jr $ra		#retorna ao sitio que chamou a função
	nop			
					

#esta label auxiliar so e utilizada quando o buffer da imagem em gray está no penultimo pixel da linha
#passa para a próxima linha
incrementa_a0_a1:
	add $t6, $zero, $zero #contador da linha volta ao 0
	addi $a0, $a0, 2 #saltar para o inicio da próxima linha
	addi $a1, $a1, 2 #saltar para o segundo pixel da proxima linha
	j mh_ciclo
	nop						

								
#função contour
# a0 - buffer matriz horizontal
# a1 - buffer matriz vertical
# a2 - buffer imagem de destino
	
contour:
	add $t9, $zero, $a1
	
contour_ciclo:	
	beq $a0, $t9, contour_sair
	nop

	lb $t0, 0($a0)
	lb $t1, 0($a1)
	
	div $t0, $t0, 2
	div $t1, $t1, 2
	add $t2, $t0, $t1 #t2 contem agora a matriz c(enunciado)
	addi $t3, $zero, 255
	sub $t2, $t3, $t2
	abs $t2, $t2
	sb $t2, 0($a2)
	
	addi $a0, $a0, 1
	addi $a1, $a1, 1
	addi $a2, $a2, 1
	
	j contour_ciclo
	nop
	
contour_sair:
	jr $ra
	nop
	  
																																												
#função print	
print:

	addi $sp,$sp-4	#Reservar espaço na pilha
	sw $ra,0($sp)	#Guardar ra na pilha
		
	li $v0, 4	#Carrega para v0 o valor da função a executar pelo sistema
	syscall		

	lw $ra,0($sp) 	#Carregar novamente o ra
	addi $sp,$sp,4	#Libertar espaço na pilha
			
	jr $ra		#retorna ao sitio que chamou a função
	nop	
	
	


#################################################################################################	
#Função para abrir o ficheiro, recebe como argumento $a0 o nome do ficheiro                     #
#################################################################################################
openfile: 		
		addi $sp, $sp, -4	#Reserva espaço na pilha
		sw $ra, 0($sp)		#reserva ra na pilha

		li $v0, 13		# Operação Syscall 
		li $a2, 0
		syscall			# Abre o ficheiro
		
		lw $ra, 0($sp)		# Repõe o valor de ra
		addi $sp, $sp, 4	# libertar espaço
		
		jr $ra
		nop
		
#################################################################################################	
#Função para ler o ficheiro							                #
#################################################################################################
#ARGUMENTOS - a0(nome do ficheiro), a1(buffer adress), a2(numero de bytes que queremos ler)
read_rgb_image:		
		addi $sp, $sp, -4	#Reserva espaço na pilha
		sw $ra, 0($sp)		#Guarda ra na pila
										
		li $v0, 14		# Syscall para ler do ficheiro
		syscall	
		
		lw $ra, 0($sp)		# Repõe o valor de ra
		addi $sp, $sp, 4	# libertar espaço
		
		jr $ra
		nop
	

##################################################################################	
#Função para fechar o ficheiro                                                   #
##################################################################################
#argumentos: a0(nome do ficheiro),a1(buffer address) , a2 (numero de bytes que queremos ler)

writefile:
		addi $sp, $sp, -4	#Reserva espaço na pilha
		sw $ra, 0($sp)		#Guarda ra na pila
	
		li $v0, 15              
		syscall
	
			
		lw $ra, 0($sp)		# Repõe o valor de ra
		addi $sp, $sp, 4	# libertar espaço
	
		jr $ra
		nop

##################################################################################	
#Função para fechar o ficheiro							 #
##################################################################################
#Argumentos: a0(nome do ficheiro)	
	
closefile:
	
	addi $sp,$sp,-4	#Reservar espaço na pilha
	sw $ra,0($sp)	#Guardar ra na pilha
	
	li $v0,16	#Carrega para v0 o numero da função que fecha o ficheiro
	syscall		
	
	lw $ra,0($sp) 	#Repõe o valor de ra
	addi $sp,$sp,4	#Liberta espaço na pilha
	
	jr $ra		#Volta para onde foi chamada
	nop


#funçao que converte uma imagem a cores RGB para uma imagem em tons de cinzento.
# a0 = buffer da img inicial
#a1 = end onde termina
rgb_to_gray:

	addi $sp,$sp,-4	#Reservar espaço na pilha
	sw $ra,0($sp)	#Guardar ra na pilha
	
	move $t7, $a1	#sitio onde começamos a guardar

ciclo_rgbtogray:
	
	beq $a0, $t7, end_rgbtogray 	#verifica se ja lemos todos os pixeis
	nop

	lbu $t1, 0($a0)		#sitio de onde lemos
	mulu $t1, $t1, 30 	#alterar o byte red para gray
	divu  $t1, $t1, 100
	
	
	addi $a0, $a0, 1 	#avançar para o byte blue
	
	lbu $t2, 0($a0)		#sitio de onde lemos
	mul $t2, $t2, 59 	#alterar o byte green para gray
	div $t2, $t2, 100
	
	
	addi $a0, $a0, 1 	#avançar para o byte blue
	
	lbu $t3, 0($a0)		#sitio de onde lemos
	mul $t3, $t3, 11 	#alterar o byte blue para gray
	div $t3, $t3, 100
	
	add $t4, $t1, $t2
	add $t4, $t4, $t3
	sb $t4, 0($a1)
	
	addi $a1, $a1, 1
	addi $a0, $a0, 1 	#avançar para o byte green novamente
	
	j ciclo_rgbtogray
	nop

	
end_rgbtogray:	
	lw $ra,0($sp) 	#Repõe o valor de ra
	addi $sp,$sp,4	#Liberta espaço na pilha
	
	jr $ra		#Volta para onde foi chamada
	nop


	
end:
	la $a0, end_string		#Carrega para a0 a string com o endereço da label pedir_ficheiro
	jal print			#Salta para a função que imprime as strings
	nop
	
