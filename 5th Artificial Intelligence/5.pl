h(mao_livre(direita),s0).
h(mao_livre(esquerda),s0).
h(bloco_grande(a),s0).
h(bloco_grande(d),s0).
h(bloco_pequeno(b),s0).
h(bloco_pequeno(c),s0).
h(mesa(a),s0).
h(mesa(b),s0).
h(em_cima_grande(d,a,direita),s0).
h(em_cima_grande(d,a,esquerda),s0).
h(em_cima_pequeno(c,b),s0).
h(so(c),s0).
h(nadaemcima(d),s0).
h(livre(c),s0).
h(livre(d),s0).

%agarrar_pequeno bloco pequeno de cima de outro bloco pequeno
h(bloco_mao(X,M),r(agarrar_pequeno1(X,M),S)):- 
	h(mao_livre(M),S),
	h(livre(X),S),
	h(so(X),S),
	h(em_cima_pequeno(X,Y),S),
	bloco_pequeno(Y),
	bloco_pequeno(X).

h(livre(Y),r(agarrar_pequeno1(X,M),S)):- 
	h(mao_livre(M),S),
	h(livre(X),S),
	h(so(X),S),
	h(em_cima_pequeno(X,Y),S),
	bloco_pequeno(Y),
	bloco_pequeno(X).

h(so(Y),r(agarrar_pequeno1(X,M),S)):- 
	h(mao_livre(M),S),
	h(livre(X),S),
	h(so(X),S),
	h(em_cima_pequeno(X,Y),S),
	bloco_pequeno(Y),
	bloco_pequeno(X).


%Agarrar num bloco pequeno que esta junto a outro pequeno em de cima da mesa 
h(bloco_mao(X,M),r(agarrar_pequeno2(X,M),S)):- 
	h(mao_livre(M),S),
	h(bloco_pequeno(X),S),
	h(bloco_pequeno(Y),S),
	h(juntos(X,Y),S),
	h(juntos(Y,X),S),
	h(livre(X),S),
	h(livre(Y),S),
	h(mesa(X),S),
	h(mesa(Y),S).

h(so(Y),r(agarrar_pequeno2(X,M),S)):- 
	h(mao_livre(M),S),
	h(bloco_pequeno(X),S),
	h(bloco_pequeno(Y),S),
	h(juntos(X,Y),S),
	h(juntos(Y,X),S),
	h(livre(X),S),
	h(livre(Y),S),
	h(mesa(X),S),
	h(mesa(Y),S).

%agarrar num bloco pequeno que está sozinho de cima da mesa 
h(bloco_mao(X,M),r(agarrar_pequeno3(X,M)):- 
	h(livre(X),S),
	h(mao_livre(M),S),
	h(mesa(X),S),
	h(so(X),S),
	h(bloco_pequeno(X),S).


%agarrar num bloco pequeno que está junto a outro em cima de um grande 
h(bloco_mao(X,M),r(agarrar_pequeno4(X,M)):- 
	h(mao_livre(M),S),
	h(bloco_pequeno(X),S),
	h(em_cima_grande(X,Z,_),S),
	h(em_cima_grande(Y,Z,_),S).

h(so(Y),r(agarrar_pequeno4(X,M)):- 
	h(mao_livre(M),S),
	h(bloco_pequeno(X),S),
	h(em_cima_grande(X,Z,_),S),
	h(em_cima_grande(Y,Z,_),S).


%agarrar num bloco pequeno que está sozinho em cima de um grande 
h(bloco_mao(X,M),r(agarrar_pequeno5(X,M),S)):- 
	h(mao_livre(M),S),
	h(bloco_pequeno(X),S),
	h(em_cima_grande(X,Z,_),S),
	h(livre(X),S),
	h(so(X),S),
	h(bloco_grande(Z),S).

h(livre(Z),r(agarrar_pequeno5(X,M),S)):- 
	h(mao_livre(M),S),
	h(bloco_pequeno(X),S),
	h(em_cima_grande(X,Z,_),S),
	h(livre(X),S),
	h(so(X),S),
	h(bloco_grande(Z),S).

%Largar um bloco pequeno sozinho na mesa
h(mesa(X),r(largar_pequeno_mesa(X,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S).

h(so(X),r(largar_pequeno_mesa(X,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S).

h(mao_livre(M),r(largar_pequeno_mesa(X,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S).

h(livre(X),r(largar_pequeno_mesa(X,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S).

%Largar um bloco pequeno colado a outro pequeno na mesa
h(mesa(X),r(largar_juntos_mesa(X,Y,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(so(Y),S),
	h(livre(Y),S),
	h(bloco_pequeno(Y),S),
	h(mesa(Y),S).

h(mao_livre(M),r(largar_juntos_mesa(X,Y,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(so(Y),S),
	h(livre(Y),S),
	h(bloco_pequeno(Y),S),
	h(mesa(Y),S).

h(livre(X),r(largar_juntos_mesa(X,Y,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(so(Y),S),
	h(livre(Y),S),
	h(bloco_pequeno(Y),S),
	h(mesa(Y),S).

h(juntos(X,Y),r(largar_juntos_mesa(X,Y,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(so(Y),S),
	h(livre(Y),S),
	h(bloco_pequeno(Y),S),
	h(mesa(Y),S).

h(juntos(Y,X),r(largar_juntos_mesa(X,Y,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(so(Y),S),
	h(livre(Y),S),
	h(bloco_pequeno(Y),S),
	h(mesa(Y),S).


%Largar um bloco pequeno em cima de um pequeno
h(mao_livre(M),r(largar_pequeno1(X,Y,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(so(Y),S),
	h(livre(Y),S),
	h(bloco_pequeno(Y),S).

h(livre(X),r(largar_pequeno1(X,Y,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(so(Y),S),
	h(livre(Y),S),
	h(bloco_pequeno(Y),S).

h(em_cima_pequeno(X,Y),r(largar_pequeno1(X,Y,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(so(Y),S),
	h(livre(Y),S),
	h(bloco_pequeno(Y),S).

h(so(X),r(largar_pequeno1(X,Y,M),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(so(Y),S),
	h(livre(Y),S),
	h(bloco_pequeno(Y),S).

%Largar um bloco pequeno em cima de um grande do lado esquerdo junto a outro
h(mao_livre(M),r(largar_pequeno2(X,Y,M,esquerda),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(bloco_grande(Y),S),
	h(livre(Y),S),
	h(em_cima_grande(B,Y,direita),S).

h(livre(X),r(largar_pequeno2(X,Y,M,esquerda),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(bloco_grande(Y),S),
	h(livre(Y),S),
	h(em_cima_grande(B,Y,direita),S).

%Largar um bloco pequeno em cima de um grande do lado direito junto a outro
h(livre(X),r(largar_pequeno3(X,Y,M,direita),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(bloco_grande(Y),S),
	h(livre(Y),S),
	h(so(B),S),
	h(em_cima_grande(B,Y,direita),S),
	h(livre(B),S).

h(mao_livre(M),r(largar_pequeno3(X,Y,M,direita),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(bloco_grande(Y),S),
	h(livre(Y),S),
	h(so(B),S),
	h(em_cima_grande(B,Y,direita),S),
	h(livre(B),S).

h(em_cima_grande(X,Y,direita),r(largar_pequeno3(X,Y,M,direita),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(bloco_grande(Y),S),
	h(livre(Y),S),
	h(so(B),S),
	h(em_cima_grande(B,Y,direita),S),
	h(livre(B),S).

h(juntos(X,B),r(largar_pequeno3(X,Y,M,direita),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(bloco_grande(Y),S),
	h(livre(Y),S),
	h(so(B),S),
	h(em_cima_grande(B,Y,direita),S),
	h(livre(B),S).

h(juntos(B,X),r(largar_pequeno3(X,Y,M,direita),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(bloco_grande(Y),S),
	h(livre(Y),S),
	h(so(B),S),
	h(em_cima_grande(B,Y,direita),S),
	h(livre(B),S).

%Largar um bloco pequeno em cima de um grande que não tem nada em cima
h(mao_livre(M),r(largar_pequeno4(X,Y,M,L),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(bloco_grande(Y),S),
	h(livre(Y),S),
	h(nadaemcima(Y),S).

h(livre(X),r(largar_pequeno4(X,Y,M,L),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(bloco_grande(Y),S),
	h(livre(Y),S),
	h(nadaemcima(Y),S).

h(em_cima_grande(X,Y,L),r(largar_pequeno4(X,Y,M,L),S)):- 
	h(bloco_pequeno(X),S),
	h(bloco_mao(X,M),S),
	h(bloco_grande(Y),S),
	h(livre(Y),S),
	h(nadaemcima(Y),S).

%Pegar um bloco grande do cima da mesa
h(bloco_mao(X, direita),r(agarrar_grande1(X),S)):- 
	h(mao_livre(direita),S),
	h(mao_livre(esquerda),S),
	h(mesa(X),S),
	h(bloco_grande(X),S),
	h(nadaemcima(X),S),
	h(livre(X),S).

h(bloco_mao(X, esquerda),r(agarrar_grande1(X),S)):- 
	h(mao_livre(direita),S),
	h(mao_livre(esquerda),S),
	h(mesa(X),S),
	h(bloco_grande(X),S),
	h(nadaemcima(X),S),
	h(livre(X),S).

%Pegar um bloco grande de cima de outro grande
h(bloco_mao(X,direita),r(agarrar_grande2(X,Y),S)):- 
	h(mao_livre(direita),S),
	h(mao_livre(esquerda),S),
	h(mao_livre(esquerda),S),
	h(em_cima_grande(X,Y,esquerda),S),
	h(em_cima_grande(X,Y,direita),S),
	h(bloco_grande(Y),S).

h(bloco_mao(X,esquerda),r(agarrar_grande2(X,Y),S)):- 
	h(mao_livre(direita),S),
	h(mao_livre(esquerda),S),
	h(mao_livre(esquerda),S),
	h(em_cima_grande(X,Y,esquerda),S),
	h(em_cima_grande(X,Y,direita),S),
	h(bloco_grande(Y),S).

h(nadaemcima(Y),r(agarrar_grande2(X,Y),S)):- 
	h(mao_livre(direita),S),
	h(mao_livre(esquerda),S),
	h(mao_livre(esquerda),S),
	h(em_cima_grande(X,Y,esquerda),S),
	h(em_cima_grande(X,Y,direita),S),
	h(bloco_grande(Y),S).


%Largar grande em cima de grande
h(nadaemcima(Y),r(largar_grande_grande(X),S)):- 
	h(bloco_grande(X),S),
	h(bloco_mao(X,direita),S),
	h(mao_livre(esquerda),S),
	h(bloco_mao(X,esquerda),S),
	h(nadaemcima(Y),S),
	h(livre(Y),S).


%%%%%%%%%%%%%%%%%
%leis de inercia%
%%%%%%%%%%%%%%%%%


%query
%h(mesa(c),S),
%h(mesa(d),S),
%h(em_cima_pequeno(b,c),S),
%h(em_cima_grande(a,d,esquerda),S),
%h(em_cima_grande(a,d,direita),S),
%h(mao_livre(direita),S),
%h(mao_livre(esquerda),S).



%S = r(agarrar_grande2(d,a),
%    r(largar_grande_mesa(d)), 
%	 r(agarrar_pequeno1(c, direita), 
%	 r(largar_pequeno_mesa(c, direita), 
%	 r(agarrar_pequeno_mesa(b),
%    r(largar_pequeno_1(b,c,direita),
%	 r(agarrar_grande1(a),
%	 r(largar_grande_grande(a,d), _G2193)))))) ;

