dim(30).

bloqueado((1,2),(1,3)).
bloqueado((1,3),(1,2)).
bloqueado((2,2),(2,3)).
bloqueado((2,3),(2,2)).
bloqueado((3,4),(4,4)).
bloqueado((4,4),(3,4)).
bloqueado((4,5),(3,5)).
bloqueado((3,5),(4,5)).

estado_inicial((18,18)).
estado_final((26,26)).

:- dynamic(visitou/2).
visitou((A,B)) :- estado_inicial((A,B)).

op((X,Y),baixo,(X,Y1),1) :- Y1 is Y+1,
							\+ visitou(X,Y1),
							asserta(visitou(X,Y1)),
						    \+ bloqueado((X,Y),(X,Y1)),
							Y1 >= 0,
							dim(D),
							Y1 < D.

op((X,Y),direita,(X1,Y),1) :-  	X1 is X+1,
								\+ visitou(X1,Y),
								asserta(visitou(X1,Y)),
								\+ bloqueado((X,Y),(X1,Y)),
							  	X1 >= 0,
							  	dim(D),
							  	X1 < D.

op((X,Y),esquerda,(X1,Y),1) :-  X1 is X-1,
								\+ visitou(X1,Y),
								asserta(visitou(X1,Y)),
								\+ bloqueado((X,Y),(X1,Y)),
								X1 >= 0,
								dim(D),
								X1 < D.

op((X,Y),cima,(X,Y1),1) :-	Y1 is Y-1,
							\+ visitou(X,Y1),
							asserta(visitou(X,Y1)),
							\+ bloqueado((X,Y),(X,Y1)),
							Y1 >= 0,
							dim(D),
							Y1 < D.

%pesquisa em largura espande mais os nós por isso é pior neste caso

%h((X,Y),R) :- estado_final((X1, Y1)),
%			  R is (X - X1).

h((X,Y),R) :- estado_final((X1, Y1)),
			  R is (X - X1) + (Y - Y1).