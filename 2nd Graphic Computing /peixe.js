function draw_backB(context){
	context.beginPath();
	context.moveTo(808, 463);
			
	context.lineTo(800, 463);
	
	context.lineTo(815, 410);
	
	context.lineTo(830, 368);
	
	context.lineTo(856, 348);
	
	context.lineTo(856, 348);
	
	context.lineTo(896, 339);

	context.lineTo(934, 362);
	
	context.lineTo(943, 387);
	
	context.lineTo(941, 421);
	
	context.lineTo(937, 451);
	
	context.lineTo(917, 478);
	
	context.lineTo(895, 495);
	
	context.lineTo(868, 501);
	
	context.lineTo(837, 494);
	
	context.lineTo(825, 477);
	context.closePath();
	context.fillStyle="orange";
	context.fill();
}

function draw_body(context){
	context.fillStyle = "orange";
	context.lineWidth = 1;
	context.beginPath();

	context.moveTo(311, 350);
	
	context.lineTo(328, 323);

	context.lineTo(332, 310);

	context.lineTo(337, 295);

	context.lineTo(344, 288);

	context.lineTo(361, 283);

	context.lineTo(408, 255);

	context.lineTo(478, 225);

	context.lineTo(490, 222);

	context.lineTo(509, 222);

	context.lineTo(550, 231);

	context.lineTo(595, 244);

	context.lineTo(595, 244);

	context.lineTo(660, 271);

	context.lineTo(762, 337);

	context.lineTo(778, 352);

	context.lineTo(807, 357);

	context.lineTo(838, 352);

	context.lineTo(853, 350);

	context.lineTo(838, 364);

	context.lineTo(824, 400);

	context.lineTo(814, 456);

	context.lineTo(813, 469);

	context.lineTo(794, 458);

	context.lineTo(747, 442);

	context.lineTo(703, 448);

	context.lineTo(617, 450);

	context.lineTo(559, 454);

	context.lineTo(475, 459);

	context.lineTo(442, 460);

	context.lineTo(418, 460);

	context.lineTo(392, 448);

	context.lineTo(367, 437);

	context.lineTo(338, 418);

	context.lineTo(317, 404);

	context.lineTo(310, 393);

	context.lineTo(307, 383);

	context.lineTo(305, 368);

	context.lineTo(310, 349);
	
	context.closePath();
	context.fill();
}

function draw_eye(context){
	context.beginPath();
	context.fillStyle= "#FF6600";
	context.arc(395.0,324.0,22.0,6.3, 0,0); 
	context.closePath();
	context.fill();

	context.beginPath(); 
	context.fillStyle= "#000000";
	context.arc(400.0,327.0,9.0,6.3, 0,0); 
	context.fill();
	context.closePath();

	context.beginPath(); 
	context.fillStyle= "#FFFFFF";
	context.arc(395.0,324.0,2.0,6.3, 0,0);
	context.closePath(); 
}


function draw_upB(context){

	context.fillStyle="orange";
	context.beginPath();
	context.moveTo(481, 222);
	context.lineTo(490, 173);
	
	context.lineTo(509, 167);
	
	context.lineTo(518, 159);
	
	context.lineTo(554, 162);
	
	context.lineTo(585, 181);

	context.lineTo(623, 226);
	
	context.lineTo(651, 247);
	
	context.lineTo(679, 236);
	
	context.lineTo(707, 220);
	
	context.lineTo(738, 230);
	
	context.lineTo(782, 258);
	
	context.lineTo(799, 294);

	context.lineTo(799, 322);
	
	context.lineTo(793, 342);
	
	context.lineTo(775, 347);
	
	context.lineTo(725, 320);
	
	context.lineTo(678, 312);

	context.lineTo(628, 260);
	
	context.lineTo(591, 255);

	context.lineTo(535, 229);
	
	context.lineTo(482, 222);
	context.closePath();
	context.fill();

}

function draw_downB(context){
	context.fillStyle = "orange";

	context.beginPath();
	context.moveTo(483, 191);
	context.lineTo(483, 191);

	context.lineTo(693, 440);

	context.lineTo(740, 435);

	context.lineTo(759, 450);

	context.lineTo(755, 472);

	context.lineTo(746, 485);

	context.lineTo(734, 495);

	context.lineTo(705, 500);

	context.lineTo(678, 499);

	context.lineTo(647, 487);

	context.lineTo(609, 463);

	context.lineTo(605, 458);

	context.lineTo(604, 438);

	context.closePath();

	context.fill();

	context.beginPath();
	context.moveTo(420, 489);

	context.lineTo(420, 489);

	context.lineTo(428, 496);

	context.lineTo(450, 503);

	context.lineTo(465, 504);

	context.lineTo(482, 526);

	context.lineTo(503, 536);

	context.lineTo(503, 536);

	context.lineTo(523, 530);

	context.lineTo(535, 526);

	context.lineTo(546, 494);

	context.lineTo(544, 453);

	context.lineTo(537, 454);

	context.lineTo(493, 452);

	context.lineTo(426, 452);
	
	context.closePath();

	context.fill();

}


function draw_mouth(c){

	c.strokeStyle = "#FF6600";
	c.beginPath();
	c.moveTo(316, 364);
	c.lineTo(327, 368);
	c.stroke();
}

function whitepart1(context){
	context.fillStyle="white";
	context.strokeStyle="black";
	context.beginPath();
	context.moveTo(412, 451);

	context.lineTo(412, 451);
	
	context.lineTo(441, 440);
	
	context.lineTo(455, 432);
	
	context.lineTo(470, 403);
	
	context.lineTo(482, 374);
	
	context.lineTo(482, 330);
	
	context.lineTo(468, 296);
	
	context.lineTo(447, 274);
	
	context.lineTo(418, 252);
	
	context.lineTo(390, 265);
	
	context.lineTo(372, 279);
	
	context.lineTo(395, 294);
	
	context.lineTo(414, 309);
	
	context.lineTo(429, 333);
	
	context.lineTo(437, 353);
	
	context.lineTo(441, 376);
	
	context.lineTo(436, 395);
	
	context.lineTo(425, 414);
	
	context.lineTo(403, 433);
	
	context.lineTo(373, 441);
	
	context.lineTo(409, 449);
	
	context.closePath();
	context.fill();
	context.stroke();
}

function whitepart2(context){
	context.fillStyle="white";
	context.strokeStyle="black";
	context.beginPath();
	
	context.moveTo(632, 233);
	context.lineTo(632, 233);
	
	context.lineTo(632, 233);
	
	context.lineTo(649, 248);
	
	context.lineTo(687, 234);

	context.lineTo(674, 260);

	context.lineTo(657, 281);
	
	context.lineTo(646, 303);
	
	context.lineTo(643, 334);
	
	context.lineTo(637, 383);
	
	context.lineTo(629, 412);
	
	context.lineTo(623, 436);
	
	context.lineTo(606, 459);
	
	context.lineTo(583, 454);
	
	context.lineTo(552, 454);
	
	context.lineTo(505, 459);
	
	context.lineTo(523, 415);
	
	context.lineTo(531, 371);
	
	context.lineTo(523, 356);
	
	context.lineTo(521, 336);
	
	context.lineTo(555, 307);
	
	context.lineTo(580, 289);
	
	context.lineTo(588, 265);
	
	context.lineTo(600, 212);

	context.closePath();
	context.fill();
	context.stroke();
}
function whitepart3(context){
	context.fillStyle="white";
	context.strokeStyle="black";
	
	context.beginPath();

	context.moveTo(765, 435);
	context.lineTo(765, 435);
	
	context.lineTo(765, 435);
	
	context.lineTo(762, 410);
	
	context.lineTo(762, 386);
	
	context.lineTo(779, 353);
	
	context.lineTo(806, 357);
	
	context.lineTo(864, 346);
	
	context.lineTo(844, 357);
	
	context.lineTo(836, 363);
	
	context.lineTo(828, 390);
	
	context.lineTo(820, 418);

	context.lineTo(815, 447);
	
	context.lineTo(815, 470);
	
	context.lineTo(818, 473);
	
	context.lineTo(799, 460);
	
	context.lineTo(773, 450);
	context.closePath();
	context.fill();
	context.stroke();
}

function draw_Binside(context){
	context.strokeStyle = "black";
	context.lineWidth=7;
	context.fillStyle = "orange";
	context.beginPath();
	context.moveTo(478, 466);
	context.lineTo(478, 466);

	context.lineTo(478, 466);
	
	context.lineTo(488, 460);
	
	context.lineTo(494, 466);
	
	context.lineTo(502, 460);
	
	context.lineTo(508, 463);
	
	context.lineTo(511, 450);
	
	context.lineTo(521, 454);
	
	context.lineTo(529, 449);
	
	context.lineTo(543, 447);
	
	context.lineTo(557, 445);
	
	context.lineTo(567, 443);
	
	context.lineTo(574, 437);
	
	context.lineTo(586, 428);
	
	context.lineTo(590, 409);
	
	context.lineTo(592, 385);
	
	context.lineTo(589, 363);
	
	context.lineTo(583, 348);
	
	context.lineTo(572, 339);
	
	context.lineTo(564, 335);
	
	context.lineTo(557, 335);
	
	context.lineTo(549, 337);
	
	context.stroke();
	context.fill();

}

function draw_cont(context){
	context.strokeStyle = "black";
	context.lineWidth=10;
	context.beginPath();
	context.moveTo(422, 487);
	context.lineTo(437, 497);
	
	context.lineTo(447, 501);
	
	context.lineTo(461, 499);
	context.stroke();


	context.lineWidth=17;
	context.beginPath();
	context.moveTo(519, 540);
	context.lineTo(538, 529);
	
	context.lineTo(545, 521);
	context.lineTo(549, 501);
	context.lineTo(549, 454);
	context.stroke();

	context.lineWidth=10;
	context.beginPath();
	context.moveTo(671, 497);
	
	context.lineTo(718, 502);
	
	context.lineTo(738, 497);
	
	context.lineTo(750, 484);
	
	context.lineTo(757, 466);
	
	context.lineTo(759, 450);
	context.stroke();

	context.lineWidth=7;
	context.beginPath();
	context.moveTo(854, 501);
	context.lineTo(878, 501);
	
	context.lineTo(896, 498);
	
	context.lineTo(908, 490);
	
	context.lineTo(922, 475);
	
	context.lineTo(934, 461);
	
	context.lineTo(940, 436);
	
	context.lineTo(937, 404);
	
	context.lineTo(942, 377);
	
	context.lineTo(932, 357);
	
	context.lineTo(900, 339);
	
	context.stroke();


	context.lineWidth=4;
	context.beginPath();
	context.moveTo(495, 171);
	context.lineTo(507, 169);
	
	context.lineTo(513, 163);
	
	context.lineTo(529, 160);
	
	context.lineTo(543, 160);
	
	context.lineTo(554, 162);
	
	context.lineTo(562, 166);
	
	context.lineTo(572, 171);
	
	context.lineTo(577, 178);
	
	context.lineTo(588, 187);
	
	context.lineTo(596, 203);
	context.stroke();

	context.lineWidth=8;
	context.beginPath();
	context.moveTo(800, 310);
	context.lineTo(796, 290);
	
	context.lineTo(781, 263);
	
	context.lineTo(754, 240);
	
	context.lineTo(738, 232);
	
	context.lineTo(724, 228);
	
	context.lineTo(700, 223);
	context.stroke();
}

function linesdraw(context){
	context.strokeStyle = "#FF6600";
	context.beginPath();
	context.moveTo(498, 215);
	context.lineTo(504, 173);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(506, 211);
	context.lineTo(519, 169);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(512, 209);
	context.lineTo(540, 167);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(520, 212);
	context.lineTo(556, 177);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(534, 220);
	context.lineTo(568, 180);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(550, 218);
	context.lineTo(579, 191);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(564, 220);
	context.lineTo(588, 199);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(578, 226);
	context.lineTo(589, 217);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(685, 262);
	context.lineTo(718, 234);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(694, 269);
	context.lineTo(728, 246);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(711, 276);
	context.lineTo(739, 252);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(723, 285);
	context.lineTo(751, 260);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(736, 293);
	context.lineTo(763, 266);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(743, 300);
	context.lineTo(770, 272);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(749, 309);
	context.lineTo(778, 285);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(765, 318);
	context.lineTo(786, 301);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(761, 316);
	context.lineTo(786, 291);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(774, 325);
	context.lineTo(791, 317);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(850, 365);
	context.lineTo(888, 346);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(866, 371);
	context.lineTo(912, 359);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(877, 379);
	context.lineTo(924, 377);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(886, 392);
	context.lineTo(934, 392);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(893, 406);
	context.lineTo(935, 404);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(895, 419);
	context.lineTo(934, 419);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(900, 429);
	context.lineTo(929, 435);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(880, 435);
	context.lineTo(916, 448);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(873, 452);
	context.lineTo(909, 463);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(861, 457);
	context.lineTo(894, 477);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(851, 464);
	context.lineTo(882, 486);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(843, 473);
	context.lineTo(868, 489);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(833, 471);
	context.lineTo(851, 493);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(740, 451);
	context.lineTo(750, 470);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(733, 457);
	context.lineTo(743, 483);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(722, 459);
	context.lineTo(733, 490);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(710, 461);
	context.lineTo(721, 493);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(695, 466);
	context.lineTo(708, 493);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(683, 467);
	context.lineTo(697, 493);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(667, 469);
	context.lineTo(684, 495);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(646, 467);
	context.lineTo(666, 493);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(524, 476);
	context.lineTo(542, 476);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(514, 478);
	context.lineTo(537, 500);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(498, 491);
	context.lineTo(532, 514);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(485, 497);
	context.lineTo(518, 527);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(474, 494);
	context.lineTo(494, 522);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(433, 476);
	context.lineTo(431, 490);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(443, 478);
	context.lineTo(443, 493);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(527, 383);
	context.lineTo(569, 359);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(538, 395);
	context.lineTo(573, 380);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(540, 387);
	context.lineTo(573, 367);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(545, 401);
	context.lineTo(573, 391);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(550, 409);
	context.lineTo(573, 409);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(549, 414);
	context.lineTo(571, 430);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(544, 422);
	context.lineTo(544, 438);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(530, 422);
	context.lineTo(539, 439);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(513, 427);
	context.lineTo(528, 440);
	context.stroke();
	context.closePath();

	context.beginPath();
	context.moveTo(505, 428);
	context.lineTo(511, 443);
	context.stroke();
	context.closePath();

}

function main(){
	var cv = document.getElementById("mycanvas");
	var c = cv.getContext("2d");				
	c.scale(0.3,0.3);
	c.translate(-20,-10);
	c.lineCap = "round";
	c.save();
	draw_body(c);
	draw_backB(c);
	draw_eye(c);
	draw_upB(c);
	draw_downB(c);
	draw_mouth(c);
	whitepart1(c);
	whitepart2(c);
	whitepart3(c);
	draw_cont(c);
	draw_Binside(c);
	linesdraw(c);
	console.log("OLA");


}