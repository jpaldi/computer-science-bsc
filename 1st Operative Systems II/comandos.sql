DROP TABLE IF EXISTS questionario CASCADE;
CREATE TABLE questionario (
  nome CHAR(64),
  numPerguntas INTEGER,
  respondido INTEGER,
  PRIMARY KEY (nome)
);

DROP TABLE IF EXISTS pergunta CASCADE;
CREATE TABLE pergunta (
  nomeQuest CHAR(64),
  pergunta CHAR(256),
  numero INTEGER,
  FOREIGN KEY (nomeQuest) REFERENCES questionario(nome),
  PRIMARY KEY (nomeQuest, numero)
);

DROP TABLE IF EXISTS resposta CASCADE;
CREATE TABLE resposta (
    nomeQuest CHAR(64),
    numero INTEGER,
    resposta INTEGER,
    frequencia INTEGER,
    FOREIGN KEY (nomeQuest) REFERENCES questionario(nome),
    PRIMARY KEY(nomeQuest, numero, resposta)
);

INSERT INTO questionario 
	VALUES ('Eng Inf',3,0);

INSERT INTO questionario 
	VALUES ('SIDA',4,0);

INSERT INTO questionario 
	VALUES ('Operadoras',3,0);

INSERT INTO questionario 
	VALUES ('Qualidade',5,0);