package trabalho_questionarios;
import java.util.Vector;

public class Questionario {
    private static int MIN_PERGUNTAS =3;
    private static int MAX_PERGUNTAS =5;
    
    
    public Vector<Pergunta> questionario;
    private String nome;
    int numero_de_perguntas;
    
    private int respondido;
    
    public Questionario(String nome, int n){
        if (n>=MIN_PERGUNTAS && n<=MAX_PERGUNTAS){
            this.nome = nome;
            questionario = new Vector<Pergunta>(n);
            numero_de_perguntas = n;
            respondido = 0;
        }
        else{
            //impossivel criar
        }
    }

    public String getNome(){
        return this.nome;
    }
    public void adicionar_pergunta(String x){
        if(questionario.size()<this.numero_de_perguntas && questionario.size()< MAX_PERGUNTAS){
            this.questionario.add(new Pergunta(x));
        }
        else{
            System.out.println("Este questionario so pode ter " +this.numero_de_perguntas+ " perguntas.");
        }
    }
    
    public void obter_perguntas(){
        if(questionario.size()<this.numero_de_perguntas){
            System.out.println("Este questionario tem que ter no minimo " + this.numero_de_perguntas + " perguntas.");
        }
        else{
            short i = 0;
            while (i < this.questionario.size()){
                //imprimir as perguntas
                System.out.println(questionario.get(i).pergunta);
                i++;
            }   
        }
    }
    
    public void obter_media(){
        if(questionario.size()<this.numero_de_perguntas){
            System.out.println("Este questionario tem que ter no minimo " + this.numero_de_perguntas + " perguntas.");
        }
        else{
            float media;
            short i = 0;
            while (i < this.questionario.size()){
                //imprimir as perguntas
                System.out.println(questionario.get(i).pergunta);
                media = questionario.get(i).media();
                System.out.println("\n media: "+ media + "\n");
                i++;
            }   
        }
    }
    
    public void responder_perguntas(int resposta, int i){
        this.questionario.get(i).responder(resposta);
    }
    
    public int respondido(){
        return this.respondido;
    }
    
    public void incrementa_respondido(){
        this.respondido += 1;
    }
}
