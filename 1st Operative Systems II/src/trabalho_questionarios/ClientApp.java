package trabalho_questionarios;
import java.util.ArrayList;

public class ClientApp {
    private String address;
    private int sPort;
    
    public ClientApp(String add, int p) {
        address= add;
    	sPort=p;
    }

    	public void menu() {
	    int lidos;
	    byte[] b= new byte[256];

            
	    try {
                Questionarios obj = (Questionarios) java.rmi.Naming.lookup("rmi://" + address + ":" + sPort + "/t1so2");

		while (true) {  // ciclo: pede ao utilizador...
		    System.out.print("\n\n --- MENU --- \n"+
				       "criar - criar um novo questionario\n"+
                                       "apagar - apagar questionario existente\n"+
                                       "repor - repor bd\n"+
                                       "responder - responder ao questionario\n"+
                                       "media - ver a media de respostas as perguntas do questionario\n"+
				       "listar - consultar questionario existente\n\n> ");
		    
		    // ler a opcao
		    lidos= System.in.read(b);
		    String op= new String(b,0,lidos-1);
		    
		    if (op.equals("criar")) {
			// ler dados

			System.out.println("inserir nome do questionario: ");
			lidos= System.in.read(b);
			String nomeQuestionario = new String(b,0,lidos-1);

			System.out.println("inserir numero de perguntas: ");
			lidos= System.in.read(b);
			String perguntas= new String(b,0,lidos-1);
                        int numPerguntas = Integer.parseInt(perguntas);
                        
                        if(numPerguntas < 3 || numPerguntas > 5)
                            System.out.println("Numero Inválido de perguntas!");
                        else{
                            obj.novoQuestionario(nomeQuestionario, numPerguntas);
                            System.out.println("Adicione as perguntas.");
                            for(int i = 1; i <= numPerguntas; i++){
                                System.out.print(i + "> ");
                                lidos= System.in.read(b);
                                String pergunta = new String(b,0,lidos-1);
                                obj.addPergunta(nomeQuestionario, pergunta, i);
                            }
                        }
                         
		    }       
		    else if (op.equals("listar")) {
                        ArrayList<String> quests = obj.listarQuestionarios();
			System.out.println("Questionarios existentes: \n");
                        for(int i = 0; i < quests.size(); i++){
                           System.out.println("- " + quests.get(i));
                        }
		    }               
                    else if (op.equals("media")) {
                        System.out.println("inserir nome do questionario:  ");
                        lidos= System.in.read(b);
			String nomeQuestionario = new String(b,0,lidos-1);
                        ArrayList<Integer> medias = obj.medias(nomeQuestionario);
                        
                        if(medias == null){
                            System.out.println("Questionário Inválido.");
                            return;
                        }
                        
                        System.out.println("\nMédias:  ");
                        
                        for(int i = 0; i < medias.size(); i++){
                            System.out.println((i+1) + ": "+ medias.get(i));
                        }
		    }                 
                    else if (op.equals("responder")) {
                        ArrayList<String> quests = obj.listarQuestionarios();
			                  System.out.println("inserir nome do questionario:  ");
                        lidos= System.in.read(b);
			                   String nomeQuestionario = new String(b,0,lidos-1);
                        boolean encontrou = false;
                        int index = 0;
                        while (index < quests.size() && !encontrou){
                            nomeQuestionario = this.aumentaTamanho(nomeQuestionario, 64);
                            if (nomeQuestionario.equals(quests.get(index))){
                                encontrou = true;
                                break;
                            }
                            index++;
                        }
                        if (encontrou){
                           ArrayList perguntas = obj.listarPerguntas(nomeQuestionario);
                           ArrayList respostas = new ArrayList<Integer>();
                           for(int i = 0; i < perguntas.size();i++){
                               System.out.println(perguntas.get(i));
                               lidos= System.in.read(b);
                               String respStr= new String(b,0,lidos-1);
                               try{
                                   int r = Integer.parseInt(respStr);
                                   if(r < 0 || r > 10){ // WAIT 
                                       System.out.println("Resposta Inválida.");
                                       return;
                                   }
                                   else
                                       respostas.add(r);       
                               }
                               catch(Exception e){
                                   System.out.println("Resposta Inválida.");
                                   return;
                               }
                           }
                           
                           obj.responder(nomeQuestionario, respostas);
                        }
                        else{
                            System.out.println("questionario não existente");
                        }
                        
		    }
                    else if (op.equals("apagar")) {
			System.out.println("inserir nome do questionario: ");
			lidos= System.in.read(b);
			String nome_questionario = new String(b,0,lidos-1);
                        obj.apagar(nome_questionario);
                        System.out.println("Questionario "+ nome_questionario +"foi apagado.");
		    }
                    
                    else if (op.equals("repor")) {
                        if (obj.repoeBD()) {
                            System.out.println("Base de dados reposta!\n");
                        } 
                        else {
                            System.out.println("Erro ao repor base de dados!\n");
                        }
                    }
                    
                    else{
                        System.out.println("Resposta não valida");
                    }
                }
            }
	    catch (Exception e) {
		e.printStackTrace();
	    }
        }
    public String aumentaTamanho(String str,int size){
        int empty = size - str.length();
        String newStr = str;
        
        for(int i = 0; i < empty; i++)
            newStr = newStr + " ";
        
        return newStr;
    }
    public static void main(String[] args){
	if (args.length < 2) {
		System.err.println("Argumentos insuficientes:  java ClientApp ADDRESS PORT");
		System.exit(1);
	}
	
	try {
		String addr= args[0];
		int p= Integer.parseInt(args[1]);	
	
		ClientApp cl= new ClientApp(addr,p);

		cl.menu( );   // interage com o utilizador
	}
	catch (Exception e) {
	    System.out.println("Problema no formato dos argumentos");
	    e.printStackTrace();

	}
    }
}
