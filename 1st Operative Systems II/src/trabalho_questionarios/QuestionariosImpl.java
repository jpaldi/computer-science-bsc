package trabalho_questionarios;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class QuestionariosImpl extends UnicastRemoteObject implements Questionarios{
    private final static String newline = "\n";
    private final static String fill = "************************************";
    Connection con = null;
    Statement stmt = null;
    
    public QuestionariosImpl(String dbhost, String db, String user, String pwd) throws java.rmi.RemoteException {
        try {
        
            Class.forName ("org.postgresql.Driver");  
            
            // url = "jdbc:postgresql://host:port/database",
            con = DriverManager.getConnection("jdbc:postgresql://"+dbhost+":5432/"+db,
                                              user,
                                              pwd);
            
            stmt = con.createStatement();

        } catch (ClassNotFoundException | SQLException e) {
            System.err.println("Erro na ligação à BD " + e);
        }
    }

    public boolean repoeBD() throws java.rmi.RemoteException {
        String s;
        StringBuilder sb = new StringBuilder();

        try {
            FileReader fr = new FileReader(new File("comandos.sql"));

            BufferedReader br = new BufferedReader(fr);

            while ((s = br.readLine()) != null) {
                sb.append(s);
            }
            br.close();

            String[] inst = sb.toString().split(";");

            for (int i = 0; i < inst.length; i++) {
                if (!inst[i].trim().equals("")) {
                    stmt.executeUpdate(inst[i]);
                    //System.out.println(">>" + inst[i]); //Mostra no terminal as instruções SQL executadas 

                }
            }
            System.out.println("[Client] Base de Dados reposta.");

        } catch (Exception e) {
            System.out.println("*** Erro : " + e.toString());
            e.printStackTrace();
            System.out.println(sb.toString());
        }
        return true;
    }
    
    public ArrayList listarQuestionarios() throws RemoteException {
        
        ArrayList lista_quest = new ArrayList();

        try {
            ResultSet rs = stmt.executeQuery("Select nome FROM questionario;");
            while (rs.next()) {

                String nome = rs.getString("nome");

                lista_quest.add(nome);
            }

            System.out.println("[Client] Questionarios listados");
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Erro ao aceder à BD");
        }

        return lista_quest;
    }
    
    public void novoQuestionario(String nome, int numPerguntas) throws RemoteException{
        try{
            stmt.executeUpdate("INSERT INTO questionario VALUES('" + nome + "\', " + numPerguntas +","+0+");");
            System.out.println("[Client] Criado questionario " + nome + "com " + numPerguntas + " perguntas.");
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("Erro de criaçao de questionário.");
        }
    }
    
    public void addPergunta(String questionario, String pergunta, int index) throws RemoteException{
        try{
            stmt.executeUpdate("INSERT INTO pergunta VALUES('" + questionario + "\', '" 
                    + pergunta + "', " + index +");");
            System.out.println("[Client] Adicionada pergunta ao questionario" + questionario + ".");
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("Erro de adição de pergunta.");
        }
    }
    
    public ArrayList listarPerguntas(String questionario) throws RemoteException{
        ArrayList lista_perguntas = new ArrayList();
        try {
            ResultSet rs = stmt.executeQuery("Select pergunta FROM pergunta WHERE "
                    + "nomeQuest = '"+questionario+"';");
            while (rs.next()) {

                String nome = rs.getString("pergunta");

                lista_perguntas.add(nome);
            }

            System.out.println("[Client] Questionarios listados");
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Erro ao aceder à BD");
        }
        
        return lista_perguntas;
    }
    
    public void responder(String nomeQuest, ArrayList respostas) throws RemoteException{
            try {
                for (int i = 0; i < respostas.size() ; i++){
                    ResultSet rs = stmt.executeQuery("Select * FROM resposta WHERE "
                        + "nomeQuest = '"+ nomeQuest + "' AND numero = "+i+" AND resposta =" + respostas.get(i) +";");
                    if(!rs.next())
                        stmt.executeUpdate("INSERT INTO resposta VALUES ('"
                                + nomeQuest + "', " + i + ", " + respostas.get(i) + ", " + 1 + ");");
                    else
                        stmt.executeUpdate("UPDATE resposta SET frequencia = frequencia + 1"
                                + "WHERE nomeQuest = '"+ nomeQuest + "' AND numero = "+ i +
                                " AND resposta = "+ respostas.get(i) + ";");
                }
                stmt.executeUpdate("UPDATE questionario SET respondido = respondido + 1 WHERE nome = "+nomeQuest+");");
            } 
            catch (SQLException e) {
                e.printStackTrace();
                System.err.println("Erro ao aceder à BD");
            }
    }
    
    public void apagar(String nomeQuest) throws RemoteException{
        try{
            stmt.executeUpdate("DELETE FROM questionario WHERE nome = '" + nomeQuest +"' ;");
            stmt.executeUpdate("DELETE FROM pergunta WHERE nomeQuest = '" + nomeQuest +"' ;");
            stmt.executeUpdate("DELETE FROM resposta WHERE nomeQuest = '" + nomeQuest +"' ;");
            System.out.println("[Client] Questionario " + nomeQuest + "apagado.");
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("Erro ao aceder à BD");
        }
    }
    
    public ArrayList medias(String nomeQuest) throws RemoteException{
        try{
            ResultSet rs = stmt.executeQuery("SELECT numPerguntas FROM questionario WHERE nome = '"+nomeQuest+"';");
            int num;
            
            if(rs.next())
                num = rs.getInt("numPerguntas");
            else
                return null;
            
            int total = 0;
            ArrayList medias = new ArrayList<Double>();
            
            for(int i = 0; i < num; i++){
                double val = 0;
                rs = stmt.executeQuery("SELECT * FROM resposta WHERE nomeQuest = '"+nomeQuest+"' "
                        + "AND numero ="+i+";");
                
                while(rs.next()){
                    total = total + rs.getInt("frequencia");
                    val = val + (rs.getInt("frequencia") * rs.getInt("resposta"));
                }
                
                val = val / total;
                medias.add(val);
                total = 0;
            }
            System.out.println("[Client] Medias do questionario " + nomeQuest + "consultadas.");
            
            return medias;
            
        }
        catch(Exception e){
            e.printStackTrace();
            System.err.println("Erro ao aceder à BD");
            
            return null;
        }
    }
}
