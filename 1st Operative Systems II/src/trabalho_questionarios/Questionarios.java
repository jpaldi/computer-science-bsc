package trabalho_questionarios;

import java.util.ArrayList;
import java.rmi.RemoteException;


public interface Questionarios extends java.rmi.Remote{
    public ArrayList listarQuestionarios() throws RemoteException;
    public boolean repoeBD() throws RemoteException;
    public void novoQuestionario(String nome, int numPerguntas) throws RemoteException;
    public void addPergunta(String questionario, String pergunta, int index) throws RemoteException;
    public ArrayList listarPerguntas(String questionario) throws RemoteException;
    public void responder(String nomeQuest, ArrayList respostas) throws RemoteException;
    public void apagar(String nomeQuest) throws RemoteException;
    public ArrayList medias(String nomeQuest) throws RemoteException;
}
