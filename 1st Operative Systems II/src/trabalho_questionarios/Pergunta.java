package trabalho_questionarios;
import java.io.Serializable;
import java.util.Vector;

public class Pergunta implements Serializable{
    String pergunta;
    private static short MAX_RESPOSTAS = 1000;
    Vector<Integer> resposta ;
    
    public Pergunta(String p){
        this.pergunta = p;
        resposta = new Vector<Integer>(MAX_RESPOSTAS);
    }
    
    public void responder(int x){
        if(x>=1 && x<= 10){
            resposta.add(x);
        }
        else{
            //resposta fora dos limites
        }
    }
    
    public float media(){
        int soma = 0;
        for(int i = 0; i<resposta.size();i++){
            soma += resposta.get(i);
        }
        
        return soma/resposta.size();
        
    }
}


