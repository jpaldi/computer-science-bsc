package trabalho_questionarios;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ServerApp {

    public static void main(String args[]) {

        if (args.length != 1) {
            System.err.println("Argumentos incorrectos, apenas aceita um argumento: porto");
            System.exit(1);
        }
       
        int sv_port = Integer.parseInt(args[0]);

        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("server_properties.properties"));
        } catch (IOException e) {
            System.out.println("Erro ao ler ficheiro .properties");
        }
        String dbhost = properties.getProperty("dbhost");
        String db = properties.getProperty("db");
        String user = properties.getProperty("user");
        String pwd = properties.getProperty("pwd");

        try {

            QuestionariosImpl obj = new QuestionariosImpl(dbhost, db, user, pwd);
            
            java.rmi.registry.Registry registry = java.rmi.registry.LocateRegistry.createRegistry(sv_port);

            registry.rebind("t2so2", obj);

            System.out.println("Servidor iniciado com sucesso no porto: " + sv_port);

            System.out.println("À espera ...");

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

}
